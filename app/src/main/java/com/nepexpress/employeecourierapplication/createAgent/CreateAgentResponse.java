package com.nepexpress.employeecourierapplication.createAgent;

import com.google.gson.annotations.SerializedName;

public class CreateAgentResponse {

    @SerializedName("Result")
       Boolean Result;

    @SerializedName("Message")
    String Message;


    public Boolean getResult() {
        return Result;
    }

    public void setResult(Boolean result) {
        Result = result;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
