package com.nepexpress.employeecourierapplication.createAgent;

public class CreateAgentContract {
    interface  View{
        void createAgentSuccess(String message);
        void createAgentFailure(String message);
    }
    interface Interactor{

        void createAgentFromAPI(String credential,CreateAgent createAgent);
    }
    interface  Presenter{

        void createAgent(String credential,CreateAgent createAgent);
    }
    interface createAgentListener{
        void createAgentSuccess(String message);
        void createAgentFailure(String message);
    }
}
