package com.nepexpress.employeecourierapplication.createAgent;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.base.BaseActivity;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;

import okhttp3.Credentials;

public class CreateAgentActivity extends BaseActivity implements CreateAgentContract.View {

    EditText agentNameEt, emailEt, passwordEt, addressEt, phoneEt, managerEt, mobileEt;
    String agentNameStr, emailStr, passwordStr, addressStr, phoneStr, managerStr, mobileStr, agentIdStr, latlngStr, addedByStr;

    TextView showLatLng, showAgentId, showAddedBy;
    SharedPrefUtil prefUtil;
    Button createUserBtn;
    CreateAgentPresenter createAgentPresenter;

    FusedLocationProviderClient mFusedLocationClient;
    private final String TAG=CreateAgentActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpAddUser();
    }



    private void setUpAddUser(){
        setContentView(R.layout.activity_create_agent);
        initView();
        defineView();
        bindView();
        createUserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(CreateAgentActivity.this, "lat lng is:"+showLatLng.getText().toString(), Toast.LENGTH_SHORT).show();
                if (validate()) {

                    showDialog("Creating User...");
                    CreateAgent createAgent = new CreateAgent();
                    createAgent.setAgentName(agentNameStr);
                    createAgent.setEmail(emailStr);
                    createAgent.setPassword(passwordStr);
                    createAgent.setAddress(addressStr);
                    createAgent.setPhone(phoneStr);
                    createAgent.setManager(managerStr);
                    createAgent.setMobile(mobileStr);
                    createAgent.setAgentId(agentIdStr);
                    createAgent.setLatang(showLatLng.getText().toString());
                    createAgent.setAddedBy(addedByStr);

                    createAgentPresenter.createAgent(
                            Credentials.basic(
                                    prefUtil.getStrings(AppText.KEY_USER_NAME),
                                    prefUtil.getStrings(AppText.KEY_USER_PASSWORD)),
                            createAgent
                    );

                }
            }
        });
    }

    private void initView() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        createAgentPresenter = new CreateAgentPresenter(this);
        prefUtil = new SharedPrefUtil(this);
    }

    private void defineView() {

        getSupportActionBar().setTitle("Create user");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        agentNameEt = findViewById(R.id.agent_name_et);
        emailEt = findViewById(R.id.email_et);
        passwordEt = findViewById(R.id.password_et);
        addressEt = findViewById(R.id.address_et);
        phoneEt = findViewById(R.id.phone_et);
        managerEt = findViewById(R.id.manager_et);
        mobileEt = findViewById(R.id.mobile_et);
        showAgentId = findViewById(R.id.show_agent_id_tv);
        showLatLng = findViewById(R.id.show_latlng_tv);
        showAddedBy = findViewById(R.id.show_added_by_tv);
        createUserBtn = findViewById(R.id.create_user_btn);
    }

    private void bindView() {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                latlngStr=location.getLatitude()+","+location.getLongitude();
                                showLatLng.setText(latlngStr);
                                Log.d(TAG,"the location is:"+latlngStr);

                            }else{
                                Log.d(TAG,"location is empty");
                            }
                        }
                    }).addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(CreateAgentActivity.this, "Failed to get location. Retry...", Toast.LENGTH_SHORT).show();
                }
            });

        String longAgentId= String.valueOf(System.nanoTime());
        agentIdStr=longAgentId.substring(longAgentId.length()-6);



        showAgentId.setText(agentIdStr);

      addedByStr=prefUtil.getStrings(AppText.KEY_USER_NAME);
      showAddedBy.setText(addedByStr);

    }
    private boolean validate(){
        boolean isValid=false;
        agentNameStr=agentNameEt.getText().toString();
        emailStr=emailEt.getText().toString();
        passwordStr=passwordEt.getText().toString();
        addressStr=addressEt.getText().toString();
        phoneStr=phoneEt.getText().toString();
        managerStr=managerEt.getText().toString();
        mobileStr=mobileEt.getText().toString();


        if(TextUtils.isEmpty(agentNameStr))
            agentNameEt.setError("Required");
        else if(TextUtils.isEmpty(emailStr))
            emailEt.setError("Required");
        else if(TextUtils.isEmpty(passwordStr))
            passwordEt.setError("Required");
        else if(TextUtils.isEmpty(addressStr))
            addressEt.setError("Required");
        else if(TextUtils.isEmpty(phoneStr))
            phoneEt.setError("Required");
        else if(TextUtils.isEmpty(managerStr))
            managerEt.setError("Required");
        else if(TextUtils.isEmpty(mobileStr))
            mobileEt.setError("Required");

        else
            isValid=true;
        return isValid;

    }

    @Override
    public void createAgentSuccess(String message) {

        hideDialog();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void createAgentFailure(String message) {

        hideDialog();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}


