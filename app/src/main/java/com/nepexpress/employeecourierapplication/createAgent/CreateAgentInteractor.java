package com.nepexpress.employeecourierapplication.createAgent;



import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAgentInteractor implements CreateAgentContract.Interactor {
    CreateAgentContract.createAgentListener createAgentListener;
    CourierService courierService;

    public CreateAgentInteractor(CreateAgentContract.createAgentListener createAgentListener) {
        this.createAgentListener = createAgentListener;
        courierService= RetrofitApiClient.getCourierService();
    }

    @Override
    public void createAgentFromAPI(String credential,CreateAgent createAgent) {

        Call<CreateAgentResponse> call=courierService.createAgent(credential,createAgent);
        call.enqueue(new Callback<CreateAgentResponse>() {
            @Override
            public void onResponse(Call<CreateAgentResponse> call, Response<CreateAgentResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().Result)
                         createAgentListener.createAgentSuccess(" "+response.body().Message);
                    else
                        createAgentListener.createAgentFailure("Failed to create agent");
                }
                else {
                        createAgentListener.createAgentFailure("Failed to create user");
                }
            }

            @Override
            public void onFailure(Call<CreateAgentResponse> call, Throwable t) {

                try
                {
                    createAgentListener.createAgentFailure("Error connecting to server");
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });
    }
}
