package com.nepexpress.employeecourierapplication.createAgent;

public class CreateAgentPresenter  implements CreateAgentContract.Presenter,CreateAgentContract.createAgentListener{

    CreateAgentContract.View view;
    CreateAgentInteractor agentInteractor;

    public CreateAgentPresenter(CreateAgentContract.View view) {
        this.view = view;
        agentInteractor=new CreateAgentInteractor(this);
    }

    @Override
    public void createAgent(String credential, CreateAgent createAgent) {
        agentInteractor.createAgentFromAPI(credential,createAgent);
    }

    @Override
    public void createAgentSuccess(String message) {

        view.createAgentSuccess(message);
    }

    @Override
    public void createAgentFailure(String message) {

        view.createAgentFailure(message);
    }
}
