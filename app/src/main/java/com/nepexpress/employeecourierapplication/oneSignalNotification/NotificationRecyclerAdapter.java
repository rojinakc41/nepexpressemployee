package com.nepexpress.employeecourierapplication.oneSignalNotification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nepexpress.employeecourierapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jeevan Acharya on 3/11/2019.
 */

public class NotificationRecyclerAdapter extends RecyclerView.Adapter<NotificationRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private List<NotificationModel> notificationModelList;

    public NotificationRecyclerAdapter(Context context) {
        this.mContext = context;
        notificationModelList = new ArrayList<>();
    }

    @Override
    public NotificationRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_notification, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(NotificationRecyclerAdapter.ViewHolder holder, int position) {
        holder.mItem = notificationModelList.get(position);
        holder.title.setText(holder.mItem.getTitle() + " ");
        holder.desc.setText(holder.mItem.getMessage() + " ");
        try {
            holder.date.setText(holder.mItem.getDate().split(" ")[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return notificationModelList == null ? 0 : notificationModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        NotificationModel mItem;
        TextView title, desc, date;

        ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.notification_title);
            desc = itemView.findViewById(R.id.notification_body);
            date = itemView.findViewById(R.id.notification_date);
        }
    }

    void setNotificationModelList(List<NotificationModel> notificationModelList) {
        this.notificationModelList = notificationModelList;
        notifyDataSetChanged();
    }
}
