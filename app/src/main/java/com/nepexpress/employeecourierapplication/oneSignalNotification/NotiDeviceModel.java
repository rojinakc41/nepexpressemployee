package com.nepexpress.employeecourierapplication.oneSignalNotification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jeevan Acharya on 3/13/2019.
 */

public class NotiDeviceModel {

    @SerializedName("StaffId")
    @Expose
    private String staffId;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("UserType")
    @Expose
    private String userType;
    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("MessageId")
    @Expose
    private String messageId;
    @SerializedName("Timestamp")
    @Expose
    private String timestamp;

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

}
