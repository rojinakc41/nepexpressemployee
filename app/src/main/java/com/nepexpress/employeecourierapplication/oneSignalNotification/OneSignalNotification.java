package com.nepexpress.employeecourierapplication.oneSignalNotification;

import android.app.Application;

import com.onesignal.OneSignal;

/**
 * Created by Ytech on 14-08-18.
 */

public class OneSignalNotification extends Application {

    public static void InitilizeOneSignal(Application application) {
        OneSignal.startInit(application)
                .setNotificationReceivedHandler(new OneSignalNotificationReceivedHandler(application))
                .setNotificationOpenedHandler(new OneSignalNotificationOpenedHandler(application))
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

    }
}
