package com.nepexpress.employeecourierapplication.oneSignalNotification;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ytech on 14-08-18.
 */

public class OneSignalNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
    private Context context;
    //    private PublicNotificationViewModel publicNotificationViewModel;
//    private PrivateNotificationViewModel privateNotificationViewModel;
    private Application application;
    CourierDatabse database;

    OneSignalNotificationReceivedHandler(Application mContext) {
        this.application = mContext;
        this.context = mContext;
    }

    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;
        String notificationID = notification.payload.notificationID;
        String title = notification.payload.title;
        String body = notification.payload.body;
        String smallIcon = notification.payload.smallIcon;
        String largeIcon = notification.payload.largeIcon;
        String bigPicture = notification.payload.bigPicture;
        String smallIconAccentColor = notification.payload.smallIconAccentColor;
        String sound = notification.payload.sound;
        String ledColor = notification.payload.ledColor;
        int lockScreenVisibility = notification.payload.lockScreenVisibility;
        String groupKey = notification.payload.groupKey;
        String groupMessage = notification.payload.groupMessage;
        String fromProjectNumber = notification.payload.fromProjectNumber;
        String rawPayload = notification.payload.rawPayload;
        String customKey;
        database = CourierDatabse.getAppDatabase(context);

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        NotificationModel model = new NotificationModel();
        model.setTitle(title);
        model.setDate(dateFormat.format(new Date()));
        model.setMessage(body);
        database.notificationDao().insertNotification(model);

        if (data != null) {
            customKey = data.optString("customkey", null);
            if (customKey != null)
                Log.i("OneSignalExample", "customkey set with value: " + customKey);
        }
    }
}

