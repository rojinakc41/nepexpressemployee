package com.nepexpress.employeecourierapplication.oneSignalNotification;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Jeevan Acharya on 3/11/2019.
 */
@Dao
public interface NotificationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNotification(NotificationModel model);

    @Query("SELECT * FROM NotificationModel Order By date DESC")
    List<NotificationModel> getAllNotification();

    @Query("DELETE FROM NotificationModel")
    void deleteAllNotification();


}
