package com.nepexpress.employeecourierapplication.oneSignalNotification;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by Jeevan Acharya on 3/11/2019.
 */

@Database(entities = NotificationModel.class, version = 3, exportSchema = false)
public abstract class NotificationDatabase extends RoomDatabase {

    public abstract NotificationDao notificationDao();

    public static String DATABASE = "CREATE TABLE IF NOT EXISTS `NotificationModel` (`id` INTEGER NOT NULL, `title` TEXT, `message` TEXT, `date` TEXT, PRIMARY KEY(`id`))";
    private static NotificationDatabase INSTANCE;

    private static final Migration MIGRATION_1_3 = new Migration(1, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(DATABASE);
        }
    };
    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(DATABASE);
        }
    };

    public static NotificationDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (NotificationDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), NotificationDatabase.class,
                            "courier-database")
                            .allowMainThreadQueries()
                            .addMigrations(MIGRATION_1_3, MIGRATION_2_3)
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
