package com.nepexpress.employeecourierapplication.oneSignalNotification;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.nepexpress.employeecourierapplication.login.LoginActivity;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by Ytech on 14-08-18.
 */

public class OneSignalNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
    private Context mContext;

    OneSignalNotificationOpenedHandler(Context context) {
        mContext = context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;
        String launchUrl = result.notification.payload.launchURL; // update docs launchUrl

        String customKey;
        String openURL = null;
        Object activityToLaunch = LoginActivity.class;

        if (data != null) {
            customKey = data.optString("customkey", null);
            openURL = data.optString("openURL", null);

            if (customKey != null)
                Log.i("OneSignalExample", "customkey set with value: " + customKey);

            if (openURL != null)
                Log.i("OneSignalExample", "openURL to webview with URL value: " + openURL);
        }

        if (actionType == OSNotificationAction.ActionType.ActionTaken) {
            Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);

            if (result.action.actionID.equals("id1")) {
                Log.i("OneSignalExample", "button id called: " + result.action.actionID);
                activityToLaunch = LoginActivity.class;
            } else
                Log.i("OneSignalExample", "button id called: " + result.action.actionID);
        }
        // The following can be used to open an Activity of your choice.
        // Replace - getApplicationContext() - with any Android Context.
        // Intent intent = new Intent(getApplicationContext(), YourActivity.class);
        Intent intent = new Intent(mContext, (Class<?>) activityToLaunch);
        // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("openURL", openURL);
        Log.i("OneSignalExample", "openURL = " + openURL);
/*
        int requestCode = 0;

            PendingIntent pendingIntent = PendingIntent.getActivity(Roshetta.app, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

            android.support.v4.app.NotificationCompat.Builder noBuilder = new android.support.v4.app.NotificationCompat.Builder(Roshetta.app)
                    .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(largeIcon).setContentTitle(result.notification.payload.title)
                    .setContentText(result.notification.payload.body )
                    .setAutoCancel(true).setDefaults(android.app.Notification.DEFAULT_ALL)
                    .setContentIntent(pendingIntent).setSound(sound);


            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
             notificationManager.notify(0, noBuilder.build()); //0 = ID of notification
*/


        // startActivity(intent);
        mContext.startActivity(intent);
    }
}
