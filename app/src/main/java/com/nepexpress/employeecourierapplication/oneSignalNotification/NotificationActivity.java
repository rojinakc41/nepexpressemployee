package com.nepexpress.employeecourierapplication.oneSignalNotification;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;

public class NotificationActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    NotificationRecyclerAdapter mAdapter;
    CourierDatabse mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initialize();
        setuppRecyclerView(recyclerView);
        observeNotificationData();
    }

    private void initialize() {
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Notification");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.recycler_view);
        assert recyclerView != null;
        mAdapter = new NotificationRecyclerAdapter(this);
        mDatabase = CourierDatabse.getAppDatabase(this);

    }

    private void setuppRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void observeNotificationData() {
        mAdapter.setNotificationModelList(mDatabase.notificationDao().getAllNotification());
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
