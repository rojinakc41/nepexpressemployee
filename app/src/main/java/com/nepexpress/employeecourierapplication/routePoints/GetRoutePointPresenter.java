package com.nepexpress.employeecourierapplication.routePoints;



import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by rojin on 1/4/2018.
 */



public class GetRoutePointPresenter implements GetRoutePointContract.Presenter,GetRoutePointContract.getRoutePointListener{

    GetRoutePointContract.View view;
    GetRoutePointInteractor routePointInteractor;

    public GetRoutePointPresenter(GetRoutePointContract.View view) {
        this.view = view;
        routePointInteractor=new GetRoutePointInteractor(this);
    }

    @Override
    public void getRoutePoints(LatLng origin, LatLng destination) {
        routePointInteractor.getRoutePointFromGoogle(origin,destination);
    }

    @Override
    public void onGetRoutePointSuccess(ArrayList<LatLng> lngArrayList, String estimatedTime, String estimatedDistance) {
        view.getRoutePointSuccess(lngArrayList,estimatedTime,estimatedDistance);
    }

    @Override
    public void onGetRoutePointFailure(String message) {
        view.getRoutePointFailure(message);
    }


}
