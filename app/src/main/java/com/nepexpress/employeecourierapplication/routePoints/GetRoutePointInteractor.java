package com.nepexpress.employeecourierapplication.routePoints;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.softech.softechtrackingpublic.api.TrackingService;

/**
 * Created by rojin on 1/4/2018.
 */

public class GetRoutePointInteractor implements GetRoutePointContract.Interactor {

    GetRoutePointContract.getRoutePointListener pointListener;
    CourierService service;
    private final String TAG=GetRoutePointInteractor.class.getSimpleName();

    public GetRoutePointInteractor(GetRoutePointContract.getRoutePointListener pointListener) {
        this.pointListener = pointListener;
        service= RetrofitApiClient.getGoogleService();
    }

    @Override
    public void getRoutePointFromGoogle(LatLng origin, LatLng destination) {
        Log.d(TAG,"getting routess");
        Call<RoutePoints> call=service.getRoutePoints(getDirectionsUrl(origin,destination));
        call.enqueue(new Callback<RoutePoints>() {
            @Override
            public void onResponse(Call<RoutePoints> call, Response<RoutePoints> response) {
                if(response.isSuccessful()){
                    ArrayList<LatLng> latLngArrayList=new ArrayList<>();
                    Log.d(TAG,"response succesful");
                    RoutePoints routePoints=response.body();
                    List<RoutePoints.Route> route=routePoints.getRoutes();
                    for(int i=0;i<route.size();i++) {
                        List<RoutePoints.Leg> legList = route.get(i).getLegs();
                        for (int j = 0; j < legList.size(); j++) {
                            List<RoutePoints.Step> stepList = legList.get(j).getSteps();
                            for (int k = 0; k < stepList.size(); k++) {
                                String polyline = "";
                                polyline = stepList.get(k).getPolyline().getPoints();
                                List<LatLng> list = decodePoly(polyline);
                                for (int l = 0; l < list.size(); l++) {
                                    latLngArrayList.add(new LatLng(list.get(l).latitude,list.get(l).longitude));
                                    Log.d(TAG,"lat lng added");
                                }
                            }
                        }
                    }

                    if(routePoints.getRoutes().size()>0) {
                        Log.d(TAG, "lat lng size is:" + latLngArrayList.size());
                        String estimatedTime = routePoints.getRoutes().get(0).getLegs().get(0).getDuration().getText();
                        String estimatedDistance = routePoints.getRoutes().get(0).getLegs().get(0).getDistance().getText();
                        pointListener.onGetRoutePointSuccess(latLngArrayList, estimatedTime, estimatedDistance);

                    }
                }else{
                    Log.d(TAG,"responsd failed");
                    pointListener.onGetRoutePointFailure("Response failed");
                }
            }

            @Override
            public void onFailure(Call<RoutePoints> call, Throwable t) {
                Log.d(TAG,"response failure");

            }
        });
    }
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "directions/json";

        // Building the url to the web service
        String url =  output + "?key=AIzaSyDo0_ianZYo8XgAZ-DV6tmgdDms0Fg9MuM&" + parameters;


        return url;
    }
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
}


