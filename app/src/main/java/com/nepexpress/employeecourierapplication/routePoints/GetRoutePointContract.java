package com.nepexpress.employeecourierapplication.routePoints;


import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by rojin on 1/4/2018.
 */

public class GetRoutePointContract {
  public   interface View{
        void getRoutePointSuccess(ArrayList<LatLng> lngArrayList,  String estimatedTime, String totalDistance);
        void getRoutePointFailure(String message);
    }
    interface Interactor{
        void getRoutePointFromGoogle(LatLng origin, LatLng destination);
    }
    interface Presenter{
        void getRoutePoints(LatLng origin, LatLng destination);

    }
    interface getRoutePointListener{
        void onGetRoutePointSuccess(ArrayList<LatLng> lngArrayList, String estimateTime, String totalDistance);
        void onGetRoutePointFailure(String message);

    }
}
