package com.nepexpress.employeecourierapplication;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Country {

    @ColumnInfo
    @SerializedName("CountryName")
    @Expose
    private String countryName;

    @ColumnInfo
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;

    @NonNull
    @PrimaryKey
    @SerializedName("Id")
    @Expose
    private String countryId;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }
}
