package com.nepexpress.employeecourierapplication.requestOrder;

public class RequestOrderPresenter implements RequestOrderContract.Presenter,RequestOrderContract.sendRequestOrderListener{

    RequestOrderInteractor requestOrderInteractor;
    RequestOrderContract.View  view;

    public RequestOrderPresenter(RequestOrderContract.View view) {
        this.view = view;
        requestOrderInteractor=new RequestOrderInteractor(this);
    }

    @Override
    public void sendRequestOrder(String credential,RequestOrder requestOrder) {
        requestOrderInteractor.sendRequestOrderToServer(credential,requestOrder);
    }

    @Override
    public void sendRequestOrderSuccess(String message) {

        view.sendRequestOrderSuccess(message);
    }

    @Override
    public void sendRequestOrderFailure(String message) {

        view.sendRequestOrderFailure(message);
    }
}
