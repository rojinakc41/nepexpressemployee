package com.nepexpress.employeecourierapplication.requestOrder;

public class RequestOrderContract {
     public interface  View{

        void sendRequestOrderSuccess(String message);
        void sendRequestOrderFailure(String message);
    }
    interface Interactor{
        void sendRequestOrderToServer(String credential,RequestOrder requestOrder);
    }
    interface Presenter{
        void sendRequestOrder(String credential,RequestOrder requestOrder);

    }
    interface sendRequestOrderListener{

        void sendRequestOrderSuccess(String message);
        void sendRequestOrderFailure(String message);
    }
}
