package com.nepexpress.employeecourierapplication.requestOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nepexpress.employeecourierapplication.Country;
import com.nepexpress.employeecourierapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RequestOrderFragment.OnRequestOrderFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RequestOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RequestOrderFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM_EMAIL = "email";
    private static final String ARG_PARAM_PASSWORD = "password";

    String orderId;
    private final String TAG=RequestOrderFragment.class.getSimpleName();

    // TODO: Rename and change types of parameters
    private String email;
    private String password;


    Button requestOrderBtn;
    private OnRequestOrderFragmentInteractionListener mListener;

    public RequestOrderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param email Parameter 1.
     * @param password Parameter 2.
     * @return A new instance of fragment RequestOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RequestOrderFragment newInstance(String email, String password) {
        RequestOrderFragment fragment = new RequestOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_EMAIL, email);
        args.putString(ARG_PARAM_PASSWORD, password);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            email = getArguments().getString(ARG_PARAM_EMAIL);
            password = getArguments().getString(ARG_PARAM_PASSWORD);
        }
        orderId= String.valueOf(System.currentTimeMillis());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_request_order, container, false);
        defineView(view);
        bindView();
        return view;
    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(RequestOrder requestOrder) {
        if (mListener != null) {
            mListener.onRequestOrderButtonPressed(requestOrder);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRequestOrderFragmentInteractionListener) {
            mListener = (OnRequestOrderFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    String receiverName,weightStr,receiverAddress,amount,receiverContact,quantity,comment,senderEmail,destination,parcel,parcelType,senderName,senderContact,senderAddress;
    EditText receiverNameEt,receiverAddressEt,amountEt,receiverContactET,
            quantityEt,commentEt,emailEt,parcelEt,
    weightEt,
            senderNameEt,senderContactEt,senderAddressEt;

    Spinner parcelTypeSpnr,destinationSpnr;
    TextView showOrderId;


    private void defineView(View view){
        requestOrderBtn=view.findViewById(R.id.request_order_btn);
        showOrderId=view.findViewById(R.id.show_order_id);
        receiverNameEt=view.findViewById(R.id.receiver_name_et);
        receiverAddressEt=view.findViewById(R.id.receiver_address_et);
        amountEt=view.findViewById(R.id.amount_et);
        receiverContactET=view.findViewById(R.id.receiver_contact_et);
        quantityEt=view.findViewById(R.id.quantity_et);
        commentEt=view.findViewById(R.id.comment_et);
        emailEt=view.findViewById(R.id.email_et);
       // agentIdEt=view.findViewById(R.id.agent_id_et);
        destinationSpnr=view.findViewById(R.id.destination_spinner);
        weightEt=view.findViewById(R.id.weight_et);
        parcelEt=view.findViewById(R.id.parcel_et);
        parcelTypeSpnr=view.findViewById(R.id.parcel_type_spinner);
        senderNameEt=view.findViewById(R.id.sender_name_et);
        senderContactEt=view.findViewById(R.id.sender_contact_et);
        senderAddressEt=view.findViewById(R.id.sender_address_et);

    }
    private void bindView(){

        showOrderId.setText(orderId);
        requestOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(validate()){
                    RequestOrder requestOrder=new RequestOrder();
                    requestOrder.setAddress(receiverAddress);
                    requestOrder.setAgentId(email);
                    requestOrder.setAmount((amount));
                    requestOrder.setComment(comment);
                    requestOrder.setContact(receiverContact);
                    requestOrder.setDestination(destination);
                    requestOrder.setName(receiverName);
                    requestOrder.setQuantity(quantity);
                    requestOrder.setEmail(senderEmail);
                    requestOrder.setOrderId(orderId);
                    requestOrder.setParcel(parcel);
                    requestOrder.setParcelType(parcelType);
                    requestOrder.setSender(senderName);
                    requestOrder.setSenderAddress(senderAddress);
                    requestOrder.setSenderContact(senderContact);
                    onButtonPressed(requestOrder);
                }

            }
        });

        weightEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d(TAG,"inside text changed");
                if(!TextUtils.isEmpty(parcelType)&&!TextUtils.isEmpty(destination)){
                    weightStr=weightEt.getText().toString();
                    if(weightStr.length()>0) {

                            caluculateAgentRate();
                        }

                    }else{
                        amountEt.setText("0");
                    }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        parcelTypeSpnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                parcelType=parcelTypeSpnr.getSelectedItem().toString();
                setUpDestinationSpinner(parcelType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        destinationSpnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    destination = destinationSpnr.getSelectedItem().toString();
                    weightStr =weightEt.getText().toString();

                    if(weightStr.length()>0)
                        caluculateAgentRate();
                    else
                        Toast.makeText(getContext(), "Enter the weight", Toast.LENGTH_SHORT).show();




            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void caluculateAgentRate(){
        if (parcelType.equalsIgnoreCase("Domestic")) {
            calculateDomesticPrice();
        } else if (parcelType.equalsIgnoreCase("International")) {
           // Toast.makeText(getContext(), "destination is:"+destination+",position is:"+i, Toast.LENGTH_SHORT).show();
            Integer p = countryNameList.indexOf(destination);
            mListener.getInternationalPrice(countries.get(p).getCountryCode(), weightStr);
        }
    }




    List<Country> countries=new ArrayList<>();
    List<String> countryNameList;
    public void setUpCountryList(List<Country> countryList){
        countries.addAll(countryList);
        countryNameList=new ArrayList<>();
        for(Country country:countryList)
            countryNameList.add(country.getCountryName());

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, countryNameList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        destinationSpnr.setAdapter(dataAdapter);

    }



    private void setUpDestinationSpinner(String parcelType){


        if(parcelType.equalsIgnoreCase("Domestic")){
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.list_of_all_districts));
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            destinationSpnr.setAdapter(dataAdapter);



        }else if(parcelType.equalsIgnoreCase("International")){
           mListener.onInternationalParcelClicked();
        }else{

        }

    }

     public void clearInputs(){

        showOrderId.setText("");
        orderId= String.valueOf(System.currentTimeMillis());
        showOrderId.setText(orderId);
        receiverNameEt.setText("");
        receiverAddressEt.setText("");

        commentEt.setText("");
        emailEt.setText("");
        amountEt.setText("");
        receiverContactET.setText("");
        quantityEt.setText("");
        weightEt.setText("");

        parcelEt.setText("");

        senderNameEt.setText("");
        senderContactEt.setText("");
        senderAddressEt.setText("");

    }
    private boolean validate(){

        boolean isValid=false;

        senderName=senderNameEt.getText().toString();
        senderContact=senderContactEt.getText().toString();
        senderAddress=senderAddressEt.getText().toString();
        senderEmail=emailEt.getText().toString();

        receiverName=receiverNameEt.getText().toString();
        receiverAddress=receiverAddressEt.getText().toString();
        receiverContact=receiverContactET.getText().toString();

        parcel=parcelEt.getText().toString();
        quantity=quantityEt.getText().toString();
        weightStr=weightEt.getText().toString();
        amount=amountEt.getText().toString();
        comment=commentEt.getText().toString();

        if(TextUtils.isEmpty(senderName))
            senderNameEt.setError("Required");
        else if(TextUtils.isEmpty(senderContact))
            senderContactEt.setError("Required");
        else if(TextUtils.isEmpty(senderAddress))
            senderAddressEt.setError("Required");
        else if(TextUtils.isEmpty(email))
            emailEt.setError("Required");

        else if(TextUtils.isEmpty(receiverName))
            receiverNameEt.setError("Required");
        else if(TextUtils.isEmpty(receiverAddress))
            receiverAddressEt.setError("Required");
        else if(TextUtils.isEmpty(receiverContact))
            receiverContactET.setError("Required");

        else if(TextUtils.isEmpty(parcel))
            parcelEt.setError("Required");
        else if(TextUtils.isEmpty(quantity))
            quantityEt.setError("Required");
        else if(TextUtils.isEmpty(amount))
            amountEt.setError("Required");
        else if(TextUtils.isEmpty(comment))
            commentEt.setError("Required");
        else if(TextUtils.isEmpty(parcelType))
            Toast.makeText(getContext(), "Please select the parcel type", Toast.LENGTH_SHORT).show();
        else
            isValid=true;

        return isValid;

    }

    private void calculateDomesticPrice(){
     /*   1 kg ko
        Amount =250 + (weight-1)*50*/

     Float weightInNo= Float.valueOf(weightStr);
     if (weightInNo<=1){
         amount="250.0";
       setAgentRate(amount);
     }else{
         Float amt=250+((weightInNo-1)*50);
        amount=""+amt;
       setAgentRate(amount);
     }
    }

    public void setAgentRate(String amt){
        Log.d(TAG,"inside setting agent rate"+amt);
        amount=amt;
        amountEt.setText(amount);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnRequestOrderFragmentInteractionListener {
        // TODO: Update argument type and name
        void onRequestOrderButtonPressed(RequestOrder requestOrder);
        void onInternationalParcelClicked();
        void getInternationalPrice(String countryCode,String totalWeight);
    }
}
