package com.nepexpress.employeecourierapplication.requestOrder;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nepexpress.employeecourierapplication.AgentRate;
import com.nepexpress.employeecourierapplication.Country;
import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;
import com.nepexpress.employeecourierapplication.base.BaseActivity;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestOrderActivity extends BaseActivity implements RequestOrderContract.View {
    String receiverName, weightStr, receiverAddress, amount, receiverContact, quantity, comment, senderEmail, destination, parcel, parcelType, senderName, senderContact, senderAddress;
    EditText receiverNameEt, receiverAddressEt, amountEt, receiverContactET,
            quantityEt, commentEt, emailEt, parcelEt,
            weightEt,
            senderNameEt, senderContactEt, senderAddressEt;

    Spinner parcelTypeSpnr, destinationSpnr;
    TextView showOrderId;
    Button requestOrderBtn;
    RequestOrderPresenter requestOrderPresenter;

    String orderId;
    SharedPrefUtil prefUtil;


    CourierService courierService;
    CourierDatabse databse;

    private String username;
    private String password;

    private final String TAG = RequestOrderActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_order);

        initView();
        defineView();
        bindView();

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


    private void initView() {
        requestOrderPresenter = new RequestOrderPresenter(this);
        courierService = RetrofitApiClient.getCourierService();
        databse = CourierDatabse.getAppDatabase(this);
        String longorderId = String.valueOf(System.nanoTime());
        orderId = longorderId.substring(longorderId.length() - 6);
        Log.d(TAG, "the total order id is:" + longorderId + ", the edited is:" + orderId);

        prefUtil = new SharedPrefUtil(this);

        username = prefUtil.getStrings(AppText.KEY_USER_NAME);
        password = prefUtil.getStrings(AppText.KEY_USER_PASSWORD);
    }

    private void defineView() {

        getSupportActionBar().setTitle("Request Order");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        requestOrderBtn = findViewById(R.id.request_order_btn);
        showOrderId = findViewById(R.id.show_order_id);
        receiverNameEt = findViewById(R.id.receiver_name_et);
        receiverAddressEt = findViewById(R.id.receiver_address_et);
        amountEt = findViewById(R.id.amount_et);
        receiverContactET = findViewById(R.id.receiver_contact_et);
        quantityEt = findViewById(R.id.quantity_et);
        commentEt = findViewById(R.id.comment_et);
        emailEt = findViewById(R.id.email_et);
        // agentIdEt=findViewById(R.id.agent_id_et);
        destinationSpnr = findViewById(R.id.destination_spinner);
        weightEt = findViewById(R.id.weight_et);
        parcelEt = findViewById(R.id.parcel_et);
        parcelTypeSpnr = findViewById(R.id.parcel_type_spinner);
        senderNameEt = findViewById(R.id.sender_name_et);
        senderContactEt = findViewById(R.id.sender_contact_et);
        senderAddressEt = findViewById(R.id.sender_address_et);

    }

    private void bindView() {

        showOrderId.setText(orderId);
        requestOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validate()) {
                    RequestOrder requestOrder = new RequestOrder();
                    requestOrder.setAddress(receiverAddress);
                    requestOrder.setAgentId(username);
                    requestOrder.setAmount(amount);
                    requestOrder.setComment(comment);
                    requestOrder.setContact(receiverContact);
                    requestOrder.setDestination(destination);
                    requestOrder.setName(receiverName);
                    requestOrder.setQuantity(quantity);
                    requestOrder.setEmail(senderEmail);
                    requestOrder.setOrderId(orderId);
                    requestOrder.setParcel(parcel);
                    requestOrder.setParcelType(parcelType);
                    requestOrder.setSender(senderName);
                    requestOrder.setSenderAddress(senderAddress);
                    requestOrder.setSenderContact(senderContact);


                    requestOrderPresenter.sendRequestOrder(Credentials.basic(username, password), requestOrder);
                    showDialog("Requesting your order");
                }

            }
        });

        weightEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d(TAG, "inside text changed");
                if (!TextUtils.isEmpty(parcelType) && !TextUtils.isEmpty(destination)) {
                    weightStr = weightEt.getText().toString();
                    if (weightStr.length() > 0) {

                        caluculateAgentRate();
                    }

                } else {
                    amountEt.setText("0");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        parcelTypeSpnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                parcelType = parcelTypeSpnr.getSelectedItem().toString();
                setUpDestinationSpinner(parcelType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        destinationSpnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                destination = destinationSpnr.getSelectedItem().toString();
                weightStr = weightEt.getText().toString();

                if (weightStr.length() > 0)
                    caluculateAgentRate();
                else
                    Toast.makeText(RequestOrderActivity.this, "Enter the weight", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void caluculateAgentRate() {
        if (parcelType.equalsIgnoreCase("Domestic") ||
                parcelType.equalsIgnoreCase("Urgent")) {
            calculateDomesticPrice();
        } else if (parcelType.equalsIgnoreCase("International")) {
            // Toast.makeText(getContext(), "destination is:"+destination+",position is:"+i, Toast.LENGTH_SHORT).show();
            Integer p = countryNameList.indexOf(destination);
            onInternationalParcelClicked(countries.get(p).getCountryCode(), weightStr);
        }
    }


    List<Country> countries = new ArrayList<>();
    List<String> countryNameList;


    public void setUpCountryList(List<Country> countryList) {
        countries.addAll(countryList);
        countryNameList = new ArrayList<>();
        for (Country country : countryList)
            countryNameList.add(country.getCountryName());

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countryNameList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        destinationSpnr.setAdapter(dataAdapter);

    }


    private void setUpDestinationSpinner(String parcelType) {


        if (parcelType.equalsIgnoreCase("Domestic")) {
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.list_of_all_districts));
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            destinationSpnr.setAdapter(dataAdapter);


        } else if (parcelType.equalsIgnoreCase("Urgent")) {

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.kathmandu_only));
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            destinationSpnr.setAdapter(dataAdapter);
        } else if (parcelType.equalsIgnoreCase("International")) {
            onInternationalParcelClicked();
        } else {

        }

    }

    private boolean validate() {

        boolean isValid = false;

        senderName = senderNameEt.getText().toString();
        senderContact = senderContactEt.getText().toString();
        senderAddress = senderAddressEt.getText().toString();
        senderEmail = emailEt.getText().toString();

        receiverName = receiverNameEt.getText().toString();
        receiverAddress = receiverAddressEt.getText().toString();
        receiverContact = receiverContactET.getText().toString();

        parcel = parcelEt.getText().toString();
        quantity = quantityEt.getText().toString();
        weightStr = weightEt.getText().toString();
        amount = amountEt.getText().toString();
        comment = commentEt.getText().toString();

        if (TextUtils.isEmpty(senderName))
            senderNameEt.setError("Required");
        else if (TextUtils.isEmpty(senderContact))
            senderContactEt.setError("Required");
        else if (TextUtils.isEmpty(senderAddress))
            senderAddressEt.setError("Required");
        else if (TextUtils.isEmpty(username))
            emailEt.setError("Required");

        else if (TextUtils.isEmpty(receiverName))
            receiverNameEt.setError("Required");
        else if (TextUtils.isEmpty(receiverAddress))
            receiverAddressEt.setError("Required");
        else if (TextUtils.isEmpty(receiverContact))
            receiverContactET.setError("Required");

        else if (TextUtils.isEmpty(parcel))
            parcelEt.setError("Required");
        else if (TextUtils.isEmpty(quantity))
            quantityEt.setError("Required");
        else if (TextUtils.isEmpty(amount))
            amountEt.setError("Required");
        else if (TextUtils.isEmpty(comment))
            commentEt.setError("Required");
        else if (TextUtils.isEmpty(parcelType))
            Toast.makeText(this, "Please select the parcel type", Toast.LENGTH_SHORT).show();
        else
            isValid = true;

        return isValid;

    }

    private void calculateDomesticPrice() {
     /*   1 kg ko
        Amount =250 + (weight-1)*50*/

        Float weightInNo = Float.valueOf(weightStr);
        if (weightInNo <= 1) {
            amount = "250.0";
            setAgentRate(amount);
        } else {
            Float amt = 250 + ((weightInNo - 1) * 50);
            amount = "" + amt;
            setAgentRate(amount);
        }
    }

    public void setAgentRate(String amt) {
        Log.d(TAG, "inside setting agent rate" + amt);
        amount = "Nrs " + amt;
        amountEt.setText(amount);
    }

    @Override
    public void sendRequestOrderSuccess(String message) {
        hideDialog();
        Toast.makeText(this, "Succesfully requested order", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void sendRequestOrderFailure(String message) {
        hideDialog();
        Toast.makeText(this, "Failed to request order", Toast.LENGTH_SHORT).show();
    }


    public void onInternationalParcelClicked(String countryCode, String totalWeight) {
        showDialog("Getting the price");
        Call<AgentRate> call = courierService.getAgentRate(Credentials.basic(username, password), countryCode, totalWeight);
        call.enqueue(new Callback<AgentRate>() {
            @Override
            public void onResponse(Call<AgentRate> call, Response<AgentRate> response) {
                hideDialog();
                if (response.isSuccessful()) {
                    amount = String.valueOf(response.body().getAmount());
                    amountEt.setText(String.valueOf(response.body().getAmount()));
                } else {
                    Log.d(TAG, "inside failed to get agent rate");

                }
            }

            @Override
            public void onFailure(Call<AgentRate> call, Throwable t) {

                try {
                    hideDialog();
                    Log.d(TAG, "inside on failed" + t.getMessage().toString());

                } catch (NullPointerException e) {

                }
            }
        });
    }

    private void fetchCountryFromServer() {
        showDialog("Loading country list");
        Call<List<Country>> call = courierService.getCountryList(Credentials.basic(username, password));
        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                hideDialog();
                if (response.isSuccessful()) {
                    databse.countryDAO().insertAllCountry(response.body());
                    setUpCountryList(response.body());
                } else {

                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {

                try {
                    hideDialog();

                } catch (NullPointerException e) {

                }
            }
        });
    }

    public void onInternationalParcelClicked() {
        //  Toast.makeText(this, "Clicked international", Toast.LENGTH_SHORT).show();
        if (databse.countryDAO().getAllCountries().size() <= 0) {
            fetchCountryFromServer();
        } else {
            setUpCountryList(databse.countryDAO().getAllCountries());
        }
    }


}
