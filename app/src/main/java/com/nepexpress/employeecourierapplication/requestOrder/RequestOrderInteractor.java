package com.nepexpress.employeecourierapplication.requestOrder;

import android.util.Log;

import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestOrderInteractor implements RequestOrderContract.Interactor {


    RequestOrderContract.sendRequestOrderListener requestOrderListener;
    CourierService courierService;
    private final String TAG=RequestOrderInteractor.class.getSimpleName();

    public RequestOrderInteractor(RequestOrderContract.sendRequestOrderListener requestOrderListener) {
        this.requestOrderListener = requestOrderListener;
        courierService= RetrofitApiClient.getCourierService();
    }

    @Override
    public void sendRequestOrderToServer(String credential, final RequestOrder requestOrder) {

        Log.d(TAG,"the request order is:"+requestOrder.getAddress());
        Call<RequestOrderResponse> call=courierService.sendRequestOrder(credential,requestOrder);
        call.enqueue(new Callback<RequestOrderResponse>() {
            @Override
            public void onResponse(Call<RequestOrderResponse> call, Response<RequestOrderResponse> response) {
                if(response.isSuccessful()) {

                    if (response.body().getResult().equalsIgnoreCase("true")) {
                        requestOrderListener.sendRequestOrderSuccess("Succesful");
                    } else {
                        requestOrderListener.sendRequestOrderFailure("Failed to upload your request");
                    }
                }else{
                    requestOrderListener.sendRequestOrderFailure("Error connecting to server");
                }
            }

            @Override
            public void onFailure(Call<RequestOrderResponse> call, Throwable t) {
                try {
                    requestOrderListener.sendRequestOrderFailure("error:"+t.getMessage().toString());
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });
    }
}
