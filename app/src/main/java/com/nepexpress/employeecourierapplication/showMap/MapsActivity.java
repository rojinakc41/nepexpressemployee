package com.nepexpress.employeecourierapplication.showMap;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.routePoints.GetRoutePointContract;
import com.nepexpress.employeecourierapplication.routePoints.GetRoutePointPresenter;
import com.nepexpress.employeecourierapplication.util.GPSTracker;

import java.util.ArrayList;

public class MapsActivity extends AppCompatActivity implements LocationListener, OnMapReadyCallback, GetRoutePointContract.View, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    SupportMapFragment mapFragment;
    ProgressDialog mProgressDialog;
    private GoogleMap mMap;
    private final String TAG = MapsActivity.class.getSimpleName();
    GetRoutePointPresenter routePointPresenter;
    public static final String ARG_SENDER_LATLNG = "senderLatLng";
    public static final String ARG_RECEIVER_LATLNG = "receiverLatLng";

    String senderLatLng, receiverLatLng;

    GoogleApiClient mLocationClient;
    //  SupportMapFragment mapFragment;
    GPSTracker gpsTracker;

    TextView showEstimatedTime, showEstimatedDistance;

    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpMapActivity();

    }


    private void setUpMapActivity() {
        setContentView(R.layout.activity_maps);
        addSupport();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (!bundle.containsKey(ARG_SENDER_LATLNG) && !bundle.containsKey(ARG_RECEIVER_LATLNG)) {

                Log.d(TAG, "inside both sender and receiver empty");
            }
            if (bundle.containsKey(ARG_RECEIVER_LATLNG)) {
                Log.d(TAG, "inside receiver la lng");
                receiverLatLng = bundle.getString(ARG_RECEIVER_LATLNG);
            }
            if (bundle.containsKey(ARG_SENDER_LATLNG)) {
                Log.d(TAG, "inside having sender lat lng");
                senderLatLng = bundle.getString(ARG_SENDER_LATLNG);

            }
        }
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 400, 1000, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER

        initView();
        defineView();
    }


    private void defineView() {
        getSupportActionBar().setTitle("Navigate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        showEstimatedTime = findViewById(R.id.show_estimated_time);
        showEstimatedDistance = findViewById(R.id.show_estimated_distance);


    }

    private void initView() {
        routePointPresenter = new GetRoutePointPresenter(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);


    }


    PolylineOptions lineOptions;

    @Override
    public void getRoutePointSuccess(ArrayList<LatLng> lngArrayList, String estimatedTime, String totalDistance) {
        hideDialog();
        lineOptions = new PolylineOptions();
        if (lineOptions != null && lngArrayList.size() > 0) {
            MarkerOptions markerOptions = new MarkerOptions().title("Sender Address")
                    .position(lngArrayList.get(0)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            MarkerOptions markerOptions1 = new MarkerOptions().title("Receiver Address")
                    .position(lngArrayList.get(lngArrayList.size() - 1)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

//            MarkerOptions markerOptions = new MarkerOptions().title("Pickup Address")
//                    .position(lngArrayList.get(0)).icon(BitmapDescriptorFactory.fromBitmap(setSenderDesIcon(R.mipmap.ic_pickup)));
//
//            MarkerOptions markerOptions1 = new MarkerOptions().title("Delivery Address")
//                    .position(lngArrayList.get(lngArrayList.size() - 1)).icon(BitmapDescriptorFactory.fromBitmap(setSenderDesIcon(R.mipmap.ic_destination)));


            mMap.addMarker(markerOptions1);
            mMap.addMarker(markerOptions);

            // Adding all the points in the route to LineOptions
            lineOptions.addAll(lngArrayList);
            lineOptions.width(10);
            lineOptions.color(Color.RED);


            mMap.addPolyline(lineOptions);

            zoomRoute(lngArrayList);
            showEstimatedDistance.setText(totalDistance);
            showEstimatedTime.setText(estimatedTime);
        } else {
            Log.d("onPostExecute", "without Polylines drawn");
        }

    }

    @Override
    public void getRoutePointFailure(String message) {
        hideDialog();

    }
 /*   private void addSupport() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        LatLng sLatLng=getLatLng(senderLatLng,senderLatLng.contains(",")?"," :";");
        LatLng rLatLng=getLatLng(receiverLatLng,receiverLatLng.contains(",")?"," :";");
        routePointPresenter.getRoutePoints(sLatLng,rLatLng);
        showDialog("loading..");

    }*/

    private LatLng getLatLng(String stringToBeSplitted, String character) {


        String[] parts = stringToBeSplitted.split(character);
        Double lat = Double.valueOf(parts[0]); // 004
        Double lng = Double.valueOf(parts[1]); // 034556

        return new LatLng(lat, lng);


    }

    protected void showDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    protected void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }


    }


    public void zoomRoute(ArrayList<LatLng> lstLatLngRoute) {


        Log.d(TAG, "zoom route size is:" + lstLatLngRoute.size());
        if (mMap == null || lstLatLngRoute == null || lstLatLngRoute.isEmpty()) return;

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : lstLatLngRoute) {
            Log.d(TAG, "lat long bounds\n" + latLngPoint.latitude + "\n" + latLngPoint.longitude);
            boundsBuilder.include(new LatLng(latLngPoint.latitude, latLngPoint.longitude));
        }

        int routePadding = 0;
        Log.d(TAG, "bounds builder:");
        LatLngBounds latLngBounds = boundsBuilder.build();
        Log.d(TAG, "center of lat ln bound is:" + latLngBounds.getCenter());


        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
        Log.d(TAG, "moved camera");


    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


    private void addSupport() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mLocationClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationClient.connect();


    }

    private Location getMyLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

            }


        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mLocationClient);
        return mLastLocation;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (senderLatLng == null) {
            Log.d(TAG, "inside any condition didnt match");
            Log.d(TAG, "inside empty");
            Location location = getMyLocation();
            if (location != null) {

                Log.d(TAG, "insiide can get location");
                senderLatLng = location.getLatitude() + "," + location.getLongitude();
                Log.d(TAG, "the sender lat lng is:" + senderLatLng);

                drawPolyline();

            } else {

                Log.d(TAG, "inside cannot get location");
                // gpsTracker.showSettingsAlert();
            }
        } else {
            drawPolyline();
        }


    }


    private void drawPolyline() {
        try {
            LatLng sLatLng = getLatLng(senderLatLng, senderLatLng.contains(",") ? "," : ";");
            LatLng rLatLng = getLatLng(receiverLatLng, receiverLatLng.contains(",") ? "," : ";");
            routePointPresenter.getRoutePoints(sLatLng, rLatLng);
            showDialog("loading..");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        try {

            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 13);
            mMap.animateCamera(cameraUpdate);
            locationManager.removeUpdates(this);
            MarkerOptions markerOptions = new MarkerOptions();

            // Setting the position for the marker
            markerOptions.position(latLng);
            //mMap.clear();
            markerOptions.title("My Location");
            try {
                Bitmap markerBitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_bike, null);
                markerBitmap = scaleBitmap(markerBitmap, 100, 100);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerBitmap));

            } catch (Exception e) {
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }
            markerOptions.getPosition();

            mMap.addMarker(markerOptions);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    private Bitmap setSenderDesIcon(int id) {
//        Bitmap markerBitmap = BitmapFactory.decodeResource(this.getResources(), id, null);
//        markerBitmap = scaleBitmap(markerBitmap, 200, 200);
//        return markerBitmap;
//
//    }

    private void setDefaultMarker(int id, String title) {
        //id ==1 receiver location and id ==2 sender loaction
        if (id == 1) {

        } else {

        }
    }

    private Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHeight / (float) bitmap.getHeight();
        float pivotX = 0;
        float pivotY = 0;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
