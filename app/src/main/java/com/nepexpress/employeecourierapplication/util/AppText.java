package com.nepexpress.employeecourierapplication.util;

/**
 * Created by rojin on 1/17/2018.
 */

public class AppText {

    //baseUrl
    public static final String BASE_URL="http://employee.nepxpress.com/api/";


    //endpoints for employee
    public static final String URL_DOWNLOAD_BASKET="Employee/DownloadBasket";
    public static final String URL_UPDATE_BASKET="Employee/UpdateBasket";
    public static final String URL_TRACK_PARCEL_EMPLOYEE="Employee/Track";
    public static final String URL_TRACK_PARCEL_AGENT="Agent/Track";
    public static final String URL_SAVE_ACTIVITY="Employee/SaveActivity";


    //endpoints for agent
    public static final String URL_REQUEST_ORDER="Agent/RequestOrder";
    public static final String URL_DOWNLOAD_BASKET_AGENT="Agent/DownloadBasket";
    public static final String URL_COUNTRY_LIST="Agent/GetCountries";
    public static final String URL_UPDATE_BASKET_AGENT="Agent/UpdateBasket";
    public static final String URL_AGENT_RATE="Agent/AgentRate";





    public static final String URL_CREATE_AGENT="User/CreateAgent";


    public static final String BASE_URL_FOR_GOOGLE="https://maps.googleapis.com/maps/api/";

    //cache strings
    public static final String KEY_USER_NAME="userName";
    public static final String KEY_USER_PASSWORD="userPassword";
    public static final String KEY_LOGIN_TYPE="loginType";



}
