package com.nepexpress.employeecourierapplication.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;


public class PreferenceUtils {

    // this is for version code
    private final String APP_VERSION_CODE = "APP_VERSION_CODE";
    private SharedPreferences sharedPreferencesAppVersionCode;
    private SharedPreferences.Editor editorAppVersionCode;
    private Context mContext;


    public PreferenceUtils(Context context) {
        this.mContext = context;
        // this is for app versioncode
        sharedPreferencesAppVersionCode = mContext.getSharedPreferences(APP_VERSION_CODE, MODE_PRIVATE);
        editorAppVersionCode = sharedPreferencesAppVersionCode.edit();
    }

    public void createAppVersionCode(float versionCode) {

        editorAppVersionCode.putFloat(APP_VERSION_CODE, versionCode);
        editorAppVersionCode.apply();
    }

    public float getAppVersionCode() {
        return sharedPreferencesAppVersionCode.getFloat(APP_VERSION_CODE, (float) 0.0); // as default version code is 0
    }
}
