package com.nepexpress.employeecourierapplication.util;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;


public class CheckForUpdate {

    public static final String ACTION_APP_VERSION_CHECK = "app-version-check";

    public static void launchPlayStoreApp(Context context) {

        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }

    public static int getRemoteVersionNumber(Context context) {
        int versionCode = 0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            float versionInternal = Float.parseFloat(pInfo.versionName);
           // versionCode = pInfo.versionCode;
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            preferenceUtils.createAppVersionCode(versionInternal);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    public class AppUpdateCheckingService extends IntentService {


        public AppUpdateCheckingService() {
            super("AppUpdateCheckingService");
        }

        /**
         * The IntentService calls this method from the default worker thread with
         * the intent that started the service. When this method returns, IntentService
         * stops the service, as appropriate.
         */
        @Override
        protected void onHandleIntent(Intent intent) {
            // Normally we would do some work here, like download a file or check for network connection.
            // For our sample, we just sleep for 1 seconds.
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                // Restore interrupt status.
                Thread.currentThread().interrupt();
            }
        }
    }
}
