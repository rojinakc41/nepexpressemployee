package com.nepexpress.employeecourierapplication.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rojin on 9/20/2017.
 */

public class SharedPrefUtil {
    private static final String APP_PREFS = "courier_preferences";

    private Context mContext;
    private SharedPreferences mSharedPreferences, sharedPreferencesPackagePick,
            sharedPreferencesPickMode, sharedPreferencesNotification, sharedPreferencesDate, sharedPreferencesTrackThread;
    private SharedPreferences.Editor mEditor, mEditorPackage, mEditorPickMode, mEditorNotification, mEditorDate, mEditorTrackStatus;


    private static final String SESSION_GET_PACKAGE_PICK = "SESSION_GET_PACKAGE_PICK";
    private static final String SESSION_MODEL = "SESSION_MODEL";

    private static final String SESSION_PICK_MODE = "SESSION_PICK_MODE";
    private static final String SESSION_PICK = "SESSION_PICK";

    private static final String SESSION_NOTIFICATION = "SESSION_NOTIFICATION";
    private static final String KEY_NOTIFICATION_POST = "DEVICEID";

    private static final String SESSION_DATE = "SESSION_DATE";
    private static final String KEY_DATE = "KEY_DATE";


    private static final String SESSION_TRACK_THREAD_RUNNING = "SESSION_TRACK_THREAD_RUNNING";
    private static final String TRACK_STATUS = "TRACK_STATUS";
    private static final String CALL_PACKAGE = "CALL_PACKAGE";


    public SharedPrefUtil(Context context) {
        this.mContext = context;

        sharedPreferencesPackagePick = mContext.getSharedPreferences(SESSION_GET_PACKAGE_PICK, Context.MODE_PRIVATE);
        mEditorPackage = sharedPreferencesPackagePick.edit();

        sharedPreferencesPickMode = mContext.getSharedPreferences(SESSION_PICK_MODE, Context.MODE_PRIVATE);
        mEditorPickMode = sharedPreferencesPickMode.edit();


        sharedPreferencesNotification = mContext.getSharedPreferences(SESSION_NOTIFICATION, Context.MODE_PRIVATE);
        mEditorNotification = sharedPreferencesNotification.edit();

        sharedPreferencesDate = mContext.getSharedPreferences(SESSION_DATE, Context.MODE_PRIVATE);
        mEditorDate = sharedPreferencesDate.edit();

        sharedPreferencesTrackThread = mContext.getSharedPreferences(SESSION_TRACK_THREAD_RUNNING, Context.MODE_PRIVATE);
        mEditorTrackStatus = sharedPreferencesTrackThread.edit();
    }


    public void saveStrings(String key, String value) {
        mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mEditor.putString(key, value);
        mEditor.commit();
    }

    public String getStrings(String key) {
        mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        return mSharedPreferences.getString(key, null);
    }

    public void insertGepPackageModelData(String model) {
        mEditorPackage.putString(SESSION_MODEL, model);
        mEditorPackage.commit();
        mEditorPackage.apply();
    }

    public String getPackageModel() {
        return sharedPreferencesPackagePick.getString(SESSION_MODEL, null);
    }

    public void insertPickMode(Boolean value) {
        mEditorPickMode.putBoolean(SESSION_PICK, value);
        mEditorPickMode.apply();
        mEditorPickMode.commit();
    }

    public void insertNotificationStatus(Boolean value) {
        mEditorNotification.putBoolean(KEY_NOTIFICATION_POST, value);
        mEditorNotification.apply();
        mEditorNotification.commit();
    }


    public void insertTodayDate(String date) {
        mEditorDate.putString(KEY_DATE, date);
        mEditorDate.apply();
        mEditorDate.commit();
    }

    public String getSessionDate() {
        return sharedPreferencesDate.getString(KEY_DATE, null);
    }

    public Boolean getNotificationStatus() {
        return sharedPreferencesNotification.getBoolean(KEY_NOTIFICATION_POST, false);
    }

    public Boolean getPickMode() {
        return sharedPreferencesPickMode.getBoolean(SESSION_PICK, false);
    }

    public void insertTrackThreadStatus(Boolean value) {
        mEditorTrackStatus.putBoolean(TRACK_STATUS, value);
        mEditorTrackStatus.commit();
        mEditorTrackStatus.apply();
    }

    public void insertPackageThreadStatus(Boolean value) {
        mEditorTrackStatus.putBoolean(CALL_PACKAGE, value);
        mEditorTrackStatus.commit();
        mEditorTrackStatus.apply();
    }

    public boolean getTrackStatus() {
        return sharedPreferencesTrackThread.getBoolean(TRACK_STATUS, false);
    }

    public boolean getPackageCallStatus() {
        return sharedPreferencesTrackThread.getBoolean(CALL_PACKAGE, false);
    }

    public static void clearUserData(Context context) {
        getSharedPreferences(context).edit().clear().commit();

    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
    }
}
