package com.nepexpress.employeecourierapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgentRate {

    @SerializedName("Id")
    @Expose
    private Double id;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;
    @SerializedName("Weight")
    @Expose
    private Double weight;
    @SerializedName("Rate")
    @Expose
    private Double rate;
    @SerializedName("Flux")
    @Expose
    private Double flux;
    @SerializedName("Amount")
    @Expose
    private Double amount;


    public Double getId() {
        return id;
    }

    public void setId(Double id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getFlux() {
        return flux;
    }

    public void setFlux(Double flux) {
        this.flux = flux;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
