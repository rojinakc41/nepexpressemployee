/*
package com.nepexpress.courierapplication.trackEmployee;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nepexpress.courierapplication.api.CourierService;
import com.nepexpress.courierapplication.api.RetrofitApiClient;
import com.nepexpress.courierapplication.mainMap.GetPackagesModel;
import com.nepexpress.courierapplication.util.AppText;
import com.nepexpress.courierapplication.util.GPSTracker;
import com.nepexpress.courierapplication.util.PreferenceUtils;
import com.nepexpress.courierapplication.util.SharedPrefUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

*/
/**
 * Created by Jeevan Acharya on 3/15/2019.
 *//*


public class PostEmployeeLiveLocation extends AsyncTask<Void, Void, Void> {

    private SharedPrefUtil sharedPrefUtil;
    private GPSTracker gps;
    private Handler handler;
    private Timer timer;

    public PostEmployeeLiveLocation(Context context) {
        sharedPrefUtil = new SharedPrefUtil(context);
        gps = new GPSTracker(context);
        handler = new Handler();
        timer = new Timer();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (sharedPrefUtil.getTrackStatus() == false) {
            sharedPrefUtil.insertTrackThreadStatus(true);
            TimerTask doAsynchronousTask = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        public void run() {
                            try {

                                String cre = Credentials.basic(sharedPrefUtil.getStrings(AppText.KEY_USER_NAME), sharedPrefUtil.getStrings(AppText.KEY_USER_PASSWORD));
                                Integer did = 0;
                                String orderId = null;
                                boolean isActive = false;
                                if (sharedPrefUtil.getPickMode() == true) {
                                    Gson gson = new Gson();
                                    GetPackagesModel model = gson.fromJson(sharedPrefUtil.getPackageModel(), GetPackagesModel.class);
                                    did = model.getDId();
                                    orderId = model.getOrderId();
                                    isActive = true;
                                }

                                CourierService service = RetrofitApiClient.getCourierService();
                                String name = sharedPrefUtil.getStrings(AppText.KEY_USER_NAME);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String date = format.format(new Date());
                                String dateTime = date.split(" ")[0] + "T" + date.split(" ")[1];
                                String latLang = 0 + "," + 0;
                                if (gps.canGetLocation()) {
                                    latLang = gps.getLatitude() + "," + gps.getLongitude();
                                }
                                Log.e("latlang", latLang);

                                service.postEmployeeLocation(cre, name, did, orderId, name, latLang, dateTime, isActive).enqueue(new Callback<TrackModel>() {
                                    @Override
                                    public void onResponse(Call<TrackModel> call, Response<TrackModel> response) {
                                        Log.e("response code track", String.valueOf(response.code()));
                                        if (response.isSuccessful()) {
                                            Log.e("isLocationUpdated", "Success true");

                                        } else {
                                            Log.e("isLocationUpdated", "Failure true");

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<TrackModel> call, Throwable t) {
                                        Log.e("isLocationUpdated", "Failure true");

                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            };
            timer.schedule(doAsynchronousTask, 0, 150000); //3 minutes
        }
        return null;
    }

}
*/
