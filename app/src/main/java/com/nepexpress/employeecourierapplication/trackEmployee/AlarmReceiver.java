package com.nepexpress.employeecourierapplication.trackEmployee;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Jeevan Acharya on 3/24/2019.
 */

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("Service_call_", "You are in AlarmReceive class.");
        Intent background = new Intent(context, TrackingService.class);
//        Intent background = new Intent(context, GoogleService.class);
        Log.e("AlarmReceive ", "testing called broadcast called");
        context.startService(background);
    }
}

