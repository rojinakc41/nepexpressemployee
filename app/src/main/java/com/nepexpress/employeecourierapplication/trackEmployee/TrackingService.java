package com.nepexpress.employeecourierapplication.trackEmployee;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;
import com.nepexpress.employeecourierapplication.mainMap.GetPackagesModel;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Credentials;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jeevan Acharya on 3/24/2019.
 */
public class TrackingService extends Service implements LocationListener {

    private static final String TAG = "TrackingService";
    private Context context;
    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    double latitude, longitude;
    LocationManager locationManager;
    Location location;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    long notify_interval = 30000;

    public double track_lat = 0.0;
    public double track_lng = 0.0;
    public double track_speed = 0.0;
    public static String str_receiver = "servicetutorial.service.receiver";
    Intent intent;
    SharedPrefUtil sharedPrefUtil;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPrefUtil = new SharedPrefUtil(this);
        mTimer = new Timer();
        mTimer.schedule(new TimerTaskToGetLocation(), 5, notify_interval);
        intent = new Intent(str_receiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        this.context = this;
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy <<");
        if (mTimer != null) {
            mTimer.cancel();
        }
    }

    private void trackLocation() {
        setModel();
        Log.e(TAG, "trackLocation");
        String TAG_TRACK_LOCATION = "trackLocation";
        Map<String, String> params = new HashMap<>();
        params.put("latitude", "" + track_lat);
        params.put("longitude", "" + track_lng);

        Log.e(TAG, "param_track_location >> " + params.toString());

        stopSelf();
        mTimer.cancel();

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /******************************/

    private void fn_getlocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnable && !isNetworkEnable) {
            Log.e(TAG, "CAN'T GET LOCATION");
            stopSelf();
        } else {
            if (isNetworkEnable) {
                location = null;
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {

                        Log.e(TAG, "isNetworkEnable latitude" + location.getLatitude() + "\nlongitude" + location.getLongitude() + "");
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        track_speed = location.getSpeed();
                        track_lat = latitude;
                        track_lng = longitude;

                        Log.e("isNWEnableLat", String.valueOf(track_lat));
                        Log.e("isNWEnableLang", String.valueOf(track_lng));
//                        fn_update(location);
                    }
                }
            }

            if (isGPSEnable) {
                location = null;
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        Log.e(TAG, "isGPSEnable latitude" + location.getLatitude() + "\nlongitude" + location.getLongitude() + "");
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        track_speed = location.getSpeed();
                        track_lat = latitude;
                        track_lng = longitude;
                        Log.e("isGpsEnableLat", String.valueOf(track_lat));
                        Log.e("isGpsEnableLang", String.valueOf(track_lng));

//                        fn_update(location);
                    }
                }
            }

            Log.e(TAG, "START SERVICE");
            trackLocation();

        }
    }

    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    fn_getlocation();
                }
            });
            //  mHandler.post(() -> fn_getlocation());

        }
    }


    public void setModel() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(new Date());
        String dateTime = date.split(" ")[0] + "T" + date.split(" ")[1];
        String username = sharedPrefUtil.getStrings(AppText.KEY_USER_NAME);
        String password = sharedPrefUtil.getStrings(AppText.KEY_USER_PASSWORD);
        if (username == null || password == null) return;
        String cre = Credentials.basic(username, password);
        Integer did = 0;
        String orderId = null;
        boolean isActive = false;
        if (sharedPrefUtil.getPickMode() == true) {
            Gson gson = new Gson();
            GetPackagesModel model = gson.fromJson(sharedPrefUtil.getPackageModel(), GetPackagesModel.class);
            did = model.getDId();
            orderId = model.getOrderId();
            isActive = true;
        }

        TrackModel trackModel = new TrackModel();
        trackModel.setLatang(track_lat + "," + track_lng);
        trackModel.setTimestamp(dateTime);
        trackModel.setDId(did);
        trackModel.setIsActive(isActive);
        trackModel.setOrderId(orderId);
        trackModel.setStaff(username);
        trackModel.setStaffId(username);
        updateLiveLocation(trackModel, cre);
    }

    private void updateLiveLocation(TrackModel trackModel, String cre) {
        CourierService service = RetrofitApiClient.getCourierService();
        service.postEmployeeLocation(cre, trackModel.getStaff(), trackModel.getDId(), trackModel.getOrderId(),
                trackModel.getStaffId(), trackModel.getLatang(), trackModel.getTimestamp(), trackModel.getIsActive()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.e("isLocationUpdated", "Success true");

                } else {
                    Log.e("isLocationUpdated", "Failure true");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("isLocationUpdated", "Failure 1 true");

            }
        });

    }
}


