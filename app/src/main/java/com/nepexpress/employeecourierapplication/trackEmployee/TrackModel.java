package com.nepexpress.employeecourierapplication.trackEmployee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jeevan Acharya on 3/15/2019.
 */

//save current locationof emplyee
public class TrackModel {

    @SerializedName("Staff")
    @Expose
    private String staff;
    @SerializedName("DId")
    @Expose
    private Integer dId;
    @SerializedName("OrderId")
    @Expose
    private String orderId;
    @SerializedName("StaffId")
    @Expose
    private String staffId;
    @SerializedName("Latang")
    @Expose
    private String latang;
    @SerializedName("Timestamp")
    @Expose
    private String timestamp;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public Integer getDId() {
        return dId;
    }

    public void setDId(Integer dId) {
        this.dId = dId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getLatang() {
        return latang;
    }

    public void setLatang(String latang) {
        this.latang = latang;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

}
