package com.nepexpress.employeecourierapplication.uploadBasket;

import android.content.Context;

import com.nepexpress.employeecourierapplication.todaysBasket.Courier;

import java.util.List;

public class UploadBasketPresenter implements UploadBasketContract.OnUploadBasketListener,UploadBasketContract.Presenter {
   UploadBasketIneractor uploadBasketIneractor;
   UploadBasketContract.View view;
Context context;
    public UploadBasketPresenter(UploadBasketContract.View view,Context context) {
        this.view = view;
        this.context= context;
        uploadBasketIneractor=new UploadBasketIneractor(this,context);
    }

    @Override
    public void uploadBasket(String credential,String url,List<Courier> uploadList) {
        uploadBasketIneractor.uploadBasketToSever(credential,url,uploadList);

    }

    @Override
    public void onUploadBasketSuccess(List<Courier> courierList) {

        view.onUploadBasketSuccess(courierList);
    }

    @Override
    public void onUploadBasketFailure(String message) {

        view.onUploadBasketFailure(message);
    }
}
