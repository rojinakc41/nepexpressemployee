package com.nepexpress.employeecourierapplication.uploadBasket;

public class CourierToUpload {
    private String Comment;
    private String Status;
    private String DeliveryDate;
    private String DID;
    private String Staff;
    private String OrderId;
    private double Distance;

    public CourierToUpload(String comment, String status, String deliveryDate, String DID, String Staff, String orderId, double distance) {
        Comment = comment;
        Status = status;
        DeliveryDate = deliveryDate;
        this.DID = DID;
        this.Staff = Staff;
        this.OrderId = orderId;
        this.Distance = distance;
    }

    public String getStaffId() {
        return Staff;
    }

    public void setStaffId(String staffId) {
        Staff = staffId;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDeliveryDate() {
        return DeliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        DeliveryDate = deliveryDate;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        this.OrderId = orderId;
    }

    public double getDistance() {
        return Distance;
    }

    public void setDistance(double distance) {
        this.Distance = distance;
    }
}
