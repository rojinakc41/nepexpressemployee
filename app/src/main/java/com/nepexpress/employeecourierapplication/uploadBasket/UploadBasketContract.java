package com.nepexpress.employeecourierapplication.uploadBasket;

import com.nepexpress.employeecourierapplication.todaysBasket.Courier;

import java.util.List;

public interface UploadBasketContract {
    interface View{
        void onUploadBasketSuccess(List<Courier> courierList);
        void onUploadBasketFailure(String message);
    }
    interface Interactor{
        void uploadBasketToSever(String credential,String url,List<Courier> uploadList);
    }
    interface Presenter{

        void uploadBasket(String credential,String url,List<Courier> uploadList);
    }
    interface OnUploadBasketListener{
        void onUploadBasketSuccess(List<Courier> courierList);
        void onUploadBasketFailure(String message);

    }
}
