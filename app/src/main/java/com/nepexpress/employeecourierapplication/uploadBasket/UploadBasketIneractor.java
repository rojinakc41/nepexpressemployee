package com.nepexpress.employeecourierapplication.uploadBasket;


import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.todaysBasket.Courier;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadBasketIneractor implements UploadBasketContract.Interactor {
    UploadBasketContract.OnUploadBasketListener uploadBasketListener;
    CourierService courierService;
    CourierDatabse courierDatabse;
    Context context;
    List<CourierToUpload> toUploadList;

    public UploadBasketIneractor(UploadBasketContract.OnUploadBasketListener uploadBasketListener, Context context) {
        this.uploadBasketListener = uploadBasketListener;
        this.context = context;
        courierService = RetrofitApiClient.getCourierService();
        courierDatabse = CourierDatabse.getAppDatabase(context);
        toUploadList = new ArrayList<>();
    }

    @Override
    public void uploadBasketToSever(String credential, String url, final List<Courier> uploadList) {


        double lat1, lat2, lang1, lang2;
        String serverKey = context.getResources().getString(R.string.google_maps_key); // Api Key For Google Direction API \\
        for (final Courier courier : uploadList) {
            //   System.out.println(" " + courier.getReceiverLatLng() + " " + courier.getSenderLatLng());
            if (courier.getSenderLatLng() != null && courier.getReceiverLatLng() != null) {

                lat1 = Double.parseDouble(courier.getSenderLatLng().split(",")[0]);
                lang1 = Double.parseDouble(courier.getSenderLatLng().split(",")[1]);
                lat2 = Double.parseDouble(courier.getReceiverLatLng().split(",")[0]);
                lang2 = Double.parseDouble(courier.getReceiverLatLng().split(",")[1]);
                LatLng startLatLng = new LatLng(lat1, lang1);
                LatLng endLatLng = new LatLng(lat2, lang2);
                double distance = SphericalUtil.computeDistanceBetween(startLatLng, endLatLng);
                distance = (distance * 1.8) / 1000;
                courier.setDistance(distance);
//
//                Log.e("ditance found is ", String.valueOf(distance));
//                // floatDistance(lat1, lang1, lat2, lang2);
//                final LatLng origin = new LatLng(lat1, lang1);
//                final LatLng destination = new LatLng(lat2, lang2);
//                //-------------Using AK Exorcist Google Direction Library---------------\\
//                GoogleDirection.withServerKey(serverKey)
//                        .from(origin)
//                        .to(destination)
//                        .transportMode(TransportMode.DRIVING)
//                        .execute(new DirectionCallback() {
//                            @Override
//                            public void onDirectionSuccess(Direction direction, String rawBody) {
//                                String status = direction.getStatus();
//                                if (status.equals(RequestResult.OK)) {
//                                    Route route = direction.getRouteList().get(0);
//                                    Leg leg = route.getLegList().get(0);
//                                    Info distanceInfo = leg.getDistance();
//                                    Info durationInfo = leg.getDuration();
//                                    String distance = distanceInfo.getText();
//                                    String duration = durationInfo.getText();
//                                    Log.e("distance", distance);
//
//                                    courier.setDistance(Double.parseDouble(distance.split(" ")[0]));
//
//                                } else if (status.equals(RequestResult.NOT_FOUND)) {
//                                    courier.setDistance(0.0);
//
//                                }
//                            }
//
//                            @Override
//                            public void onDirectionFailure(Throwable t) {
//                                courier.setDistance(0.0);
//
//                            }
//                        });
//
//                System.out.println("" + courier.getComment());
//                System.out.println("" + courier.getStatus());
//                System.out.println("" + courier.getDate());
//                System.out.println("" + courier.getDId());
//                System.out.println("" + courier.getStaff());
//                System.out.println("" + courier.getOrderId());
//                System.out.println("" + courier.getDistance());
//

                toUploadList.add(new
                        CourierToUpload(courier.getComment(), courier.getStatus(), courier.getDate(), courier.getDId(), courier.getStaff(), courier.getOrderId(), courier.getDistance()));

            } else {
                courier.setDistance(0.0);
                toUploadList.add(new
                        CourierToUpload(courier.getComment(), courier.getStatus(), courier.getDate(), courier.getDId(), courier.getStaff(), courier.getOrderId(), courier.getDistance()));
            }
        }
        Call<UploadBasketResponse> call = courierService.sendCouriers(credential, url, toUploadList);
        Log.e("upload list  size", String.valueOf(toUploadList.size()));
        call.enqueue(new Callback<UploadBasketResponse>() {
            @Override
            public void onResponse(Call<UploadBasketResponse> call, Response<UploadBasketResponse> response) {
                if (response.isSuccessful()) {
                    uploadBasketListener.onUploadBasketSuccess(uploadList);
                } else {
                    uploadBasketListener.onUploadBasketFailure("Failed to upload retry");
                }
            }

            @Override
            public void onFailure(Call<UploadBasketResponse> call, Throwable t) {

                try {
                    uploadBasketListener.onUploadBasketFailure("error:" + t.getMessage().toString());

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static void floatDistance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);
        Log.e("distance new ", String.valueOf(dist));
        //return dist;
    }
}
