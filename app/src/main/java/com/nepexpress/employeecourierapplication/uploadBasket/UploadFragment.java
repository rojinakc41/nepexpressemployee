package com.nepexpress.employeecourierapplication.uploadBasket;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.history.ParcelHistoryAdapter;
import com.nepexpress.employeecourierapplication.todaysBasket.Courier;

import java.util.List;

import okhttp3.Credentials;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UploadFragment.OnUploadFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UploadFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UploadFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_USERNAME = "username";
    private static final String ARG_PASSWORD = "password";

    // TODO: Rename and change types of parameters
    private String usernameStr;
    private String passwordStr;

    RecyclerView showCourierList;
    Button uploadCourierbtn;
    CourierDatabse courierDatabse;
    TextView showMessageTv;

    private OnUploadFragmentInteractionListener mListener;

    public UploadFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param username Parameter 1.
     * @param password Parameter 2.
     * @return A new instance of fragment UploadFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UploadFragment newInstance(String username, String password) {
        UploadFragment fragment = new UploadFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USERNAME, username);
        args.putString(ARG_PASSWORD, password);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            usernameStr = getArguments().getString(ARG_USERNAME);
            passwordStr = getArguments().getString(ARG_PASSWORD);
        }
        initView();
    }
    private void initView(){
        courierDatabse=CourierDatabse.getAppDatabase(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_upload, container, false);
        defineView(view);bindView();
        return view;
    }

    private void defineView(View view){
        showCourierList=view.findViewById(R.id.show_courier_list);
        uploadCourierbtn=view.findViewById(R.id.uplodad_couriers_btn);
       // showMessageTv=view.findViewById(R.id.show_message_tv);
    }

    private void bindView(){
        LinearLayoutManager layoutManager=new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        showCourierList.setLayoutManager(layoutManager);

        final List<Courier> courierList=courierDatabse.courierDAO().getCourierListOrderFulfilled();

        if(courierList.size()>0) {


            ParcelHistoryAdapter parcelHistoryAdapter = new ParcelHistoryAdapter(courierList);
            showCourierList.setAdapter(parcelHistoryAdapter);

            uploadCourierbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onButtonPressed(courierList);
                }
            });
        }
    }
    

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(List<Courier> courierListToUpload ) {
        if (mListener != null) {
            String credentials = Credentials.basic(usernameStr, passwordStr);
            mListener.onUploadClicked(credentials,courierListToUpload);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnUploadFragmentInteractionListener) {
            mListener = (OnUploadFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnUploadFragmentInteractionListener {
        // TODO: Update argument type and name
        void onUploadClicked(String credential,List<Courier> courierListToUpload);
    }
}
