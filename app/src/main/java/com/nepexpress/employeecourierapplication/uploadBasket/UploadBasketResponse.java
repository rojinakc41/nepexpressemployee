package com.nepexpress.employeecourierapplication.uploadBasket;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadBasketResponse {




    @SerializedName("Result")
    @Expose
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @SerializedName("Message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
