package com.nepexpress.employeecourierapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;
import com.nepexpress.employeecourierapplication.createAgent.CreateAgentActivity;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.login.LoginActivity;
import com.nepexpress.employeecourierapplication.requestOrder.RequestOrderActivity;
import com.nepexpress.employeecourierapplication.todaysBasket.Courier;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketContract;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketDetailActivity;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketFragment;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketPresenter;
import com.nepexpress.employeecourierapplication.trackParcel.TrackParcelActivity;
import com.nepexpress.employeecourierapplication.uploadBasket.UploadBasketContract;
import com.nepexpress.employeecourierapplication.uploadBasket.UploadBasketPresenter;
import com.nepexpress.employeecourierapplication.uploadBasket.UploadFragment;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;

import java.util.List;

public class AgentMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,

        UploadBasketContract.View, UploadFragment.OnUploadFragmentInteractionListener,
        TodaysBasketContract.View, TodaysBasketFragment.OnTodayBasketFragmentInteractionListener {

    Toolbar toolbar;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;

    SharedPrefUtil prefUtil;
    ProgressDialog mProgressDialog;

    private final String TAG = AgentMainActivity.class.getSimpleName();


    UploadBasketPresenter uploadBasketPresenter;
    TodaysBasketPresenter todaysBasketPresenter;

    String userNameStr, passwordStr, loginTypeStr;
    Button downloadBtn;


    CourierDatabse databse;
    CourierService courierService;

    TextView showUserNameTv, showNameInSideNav, showTotalCourierTv, showDeliveredCourierTv, showCanceledCourierTv, showPendingCourierTv, showMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_main);
        initView();
        defineView();
        bindView();
    }


    private void initView() {
        prefUtil = new SharedPrefUtil(this);
        userNameStr = prefUtil.getStrings(AppText.KEY_USER_NAME);
        passwordStr = prefUtil.getStrings(AppText.KEY_USER_PASSWORD);
        loginTypeStr = prefUtil.getStrings(AppText.KEY_LOGIN_TYPE);

        databse = CourierDatabse.getAppDatabase(getApplicationContext());
        todaysBasketPresenter = new TodaysBasketPresenter(this);
        uploadBasketPresenter = new UploadBasketPresenter(this,this);
        courierService = RetrofitApiClient.getCourierService();

    }

    private void defineView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        downloadBtn = findViewById(R.id.download_btn);

        showUserNameTv = findViewById(R.id.show_user_name);
        showTotalCourierTv = findViewById(R.id.show_total_courier);
        showDeliveredCourierTv = findViewById(R.id.show_delivered_courier);
        showCanceledCourierTv = findViewById(R.id.show_rejected_courier);
        showPendingCourierTv = findViewById(R.id.show_pending_courier);

        showMessage = findViewById(R.id.show_message);
    }

    private void bindView() {
        setSupportActionBar(toolbar);


        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        showNameInSideNav = (TextView) header.findViewById(R.id.show_username_nav);
        showNameInSideNav.setText(userNameStr);

        showUserNameTv.setText(userNameStr);

        if (databse.courierDAO().getAllCourier().size() > 0) {
            downloadBtn.setVisibility(View.GONE);
            showMessage.setVisibility(View.GONE);
            TodaysBasketFragment todaysBasketFragment = new TodaysBasketFragment();
            FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
            fragmentManager.replace(R.id.main_container, todaysBasketFragment).commit();

            setUpCouriers();
        } else {
            downloadBtn.setVisibility(View.VISIBLE);
            showMessage.setVisibility(View.VISIBLE);

            showMessage.setText("Seem like you donot have any couriers!! Download it!!!");
            downloadBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog("Loading your courier");
                    todaysBasketPresenter.getTodayBasket(userNameStr, passwordStr, AppText.URL_DOWNLOAD_BASKET_AGENT);
                }
            });
        }


    }

    private void setUpCouriers() {

        //  Toast.makeText(this, "username is:"+userNameStr, Toast.LENGTH_SHORT).show();

        showTotalCourierTv.setText(String.valueOf(databse.courierDAO().getAllCourier().size()));
        showCanceledCourierTv.setText(String.valueOf(databse.courierDAO().getNumberOfCanceledAndRejectedCourier()));
        showDeliveredCourierTv.setText(String.valueOf(databse.courierDAO().getNumberOfDeliveredCourier()));
        showPendingCourierTv.setText(String.valueOf(databse.courierDAO().getNumberOfPendingCourier()));

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
        // Toast.makeText(this, "refreshed", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onGetBasketListSuccess(List<Courier> courierList) {
        databse.courierDAO().deleteAllCourier();
        hideDialog();
        databse.courierDAO().insertAllCourier(courierList);
        finish();
        startActivity(getIntent());
    }

    @Override
    public void onGetBasketListFailure(String message) {
        hideDialog();
        Toast.makeText(this, "Couldn't get any response from tne server", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onCourierClicked(Courier courier) {
        Intent intent = new Intent(AgentMainActivity.this, TodaysBasketDetailActivity.class);
        intent.putExtra(TodaysBasketDetailActivity.KEY_PARCEL_DETAIL, courier);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.agent_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {

            TodaysBasketFragment todaysBasketFragment = new TodaysBasketFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_container, todaysBasketFragment);
            fragmentTransaction.commit();

            // Handle the camera action
        } else if (id == R.id.nav_request_pickup) {

            Intent intent = new Intent(this, RequestOrderActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_upload) {

            if (databse.courierDAO().getCourierListOrderFulfilled().size() > 0) {
                showMessage.setVisibility(View.GONE);
                UploadFragment uploadFragment = new UploadFragment().newInstance(userNameStr, passwordStr);
                FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
                fragmentManager.replace(R.id.main_container, uploadFragment).commit();
            } else {

                Toast.makeText(this, "Seems like you haven't done anything to upload courier", Toast.LENGTH_SHORT).show();

            }

        } else if (id == R.id.nav_track) {
            Intent intent = new Intent(AgentMainActivity.this, TrackParcelActivity.class);
            intent.putExtra(TrackParcelActivity.TRACK_PARCEL_URL, AppText.URL_TRACK_PARCEL_AGENT);
            startActivity(intent);

        } else if (id == R.id.nav_user) {
            Intent intent = new Intent(this, CreateAgentActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            prefUtil.saveStrings(AppText.KEY_USER_NAME, "");
            prefUtil.saveStrings(AppText.KEY_USER_PASSWORD, "");
            prefUtil.saveStrings(AppText.KEY_LOGIN_TYPE, "");
            databse.courierDAO().deleteAllCourier();
            startActivity(new Intent(AgentMainActivity.this, LoginActivity.class));
            finish();
            Toast.makeText(this, "Succesfully Logged Out", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    protected void showDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    protected void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }


    }

    @Override
    public void onUploadBasketSuccess(List<Courier> courierList) {
        hideDialog();

        Toast.makeText(this, "success:", Toast.LENGTH_SHORT).show();

        for (Courier courier : courierList) {
            databse.courierDAO().deleteIndividualCourier(courier);
        }

        finish();
        startActivity(getIntent());
    }

    @Override
    public void onUploadBasketFailure(String message) {
        hideDialog();
        Toast.makeText(this, "failed:" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUploadClicked(String credential, List<Courier> courierListToUpload) {
        showDialog("Uploading your couriers.....");
        uploadBasketPresenter.uploadBasket(credential, AppText.URL_UPDATE_BASKET_AGENT, courierListToUpload);
    }


}
