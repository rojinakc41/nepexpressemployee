package com.nepexpress.employeecourierapplication.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.AppLaunchChecker;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.nepexpress.employeecourierapplication.AgentMainActivity;
import com.nepexpress.employeecourierapplication.EmployeeMainActivity;
import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.oneSignalNotification.OneSignalNotification;
import com.nepexpress.employeecourierapplication.requestOrder.RequestOrder;
import com.nepexpress.employeecourierapplication.todaysBasket.Courier;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketContract;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketPresenter;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.CheckForUpdate;
import com.nepexpress.employeecourierapplication.util.PreferenceUtils;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import okhttp3.Credentials;

public class LoginActivity extends AppCompatActivity implements TodaysBasketContract.View {

    EditText usernameEt, passwordEt;
    Button signinBtn;
    Spinner loginTypeSpnr;
    String loginTypeStr;
    //   CourierService courierService;
    CourierDatabse database;
    private final String TAG = LoginActivity.class.getSimpleName();
    TodaysBasketPresenter basketPresenter;

    String username, password;
    SharedPrefUtil prefUtil;
    ProgressDialog mProgressDialog;
    Context activityContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activityContext = this;
        setContentView(R.layout.activity_login);
        //one signal notification
        OneSignalNotification.InitilizeOneSignal(this.getApplication());

        initView();
        requestLocationPermission();
        if (TextUtils.isEmpty(prefUtil.getStrings(AppText.KEY_USER_PASSWORD)) && TextUtils.isEmpty(prefUtil.getStrings(AppText.KEY_USER_NAME))) {
            defineView();
            bindView();
            addClickListener();
        } else {
            navigateToMain();
        }
        AppLaunchChecker.onActivityCreate(this);
        alertAppUpdate();


    }

    private void navigateToMain() {
        if (prefUtil.getStrings(AppText.KEY_LOGIN_TYPE).equalsIgnoreCase("Employee")) {
            startActivity(new Intent(LoginActivity.this, EmployeeMainActivity.class));
            finish();
        } else {
            startActivity(new Intent(LoginActivity.this, AgentMainActivity.class));
            finish();
        }
    }

    private void initView() {
        //  courierService = RetrofitApiClient.getCourierService();
        database = CourierDatabse.getAppDatabase(getApplicationContext());
        basketPresenter = new TodaysBasketPresenter(this);

        prefUtil = new SharedPrefUtil(this);
        prefUtil.insertTrackThreadStatus(false);
        prefUtil.insertPackageThreadStatus(false);
    }

    private void defineView() {
        loginTypeSpnr = findViewById(R.id.login_type);
        usernameEt = findViewById(R.id.username_et);
        passwordEt = findViewById(R.id.password_et);
        signinBtn = findViewById(R.id.login_btn);

    }

    private void bindView() {

        loginTypeSpnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loginTypeStr = loginTypeSpnr.getSelectedItem().toString();
                //  Toast.makeText(LoginActivity.this, "type:"+loginTypeStr, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void addClickListener() {
        signinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    showDialog("Logging in");
                    if (loginTypeStr.equalsIgnoreCase("Agent")) {
                        RequestOrder requestOrder = new RequestOrder();
                        String credential = Credentials.basic(username, password);
                        basketPresenter.getTodayBasket(username, password, AppText.URL_DOWNLOAD_BASKET_AGENT);
                        Log.d(TAG, "inside agent");
                    } else if (loginTypeStr.equalsIgnoreCase("Employee")) {
                        basketPresenter.getTodayBasket(username, password, AppText.URL_DOWNLOAD_BASKET);
                        Log.d(TAG, "inside employee");
                    }

                }
            }
        });
    }

    private boolean validate() {
        boolean isValid = false;
        username = usernameEt.getText().toString();
        password = passwordEt.getText().toString();
        if (TextUtils.isEmpty(username)) {
            usernameEt.setError("Required");
        } else if (TextUtils.isEmpty(password)) {
            passwordEt.setError("Required");
        } else if (TextUtils.isEmpty(loginTypeStr)) {
            Toast.makeText(this, "Please select whether you are a agent or an employee", Toast.LENGTH_SHORT).show();
        } else {
            isValid = true;
        }
        return isValid;
    }

    @Override
    public void onGetBasketListSuccess(List<Courier> courierList) {
        database.courierDAO().insertAllCourier(courierList);
        prefUtil.saveStrings(AppText.KEY_USER_NAME, username);
        prefUtil.saveStrings(AppText.KEY_USER_PASSWORD, password);
        prefUtil.saveStrings(AppText.KEY_LOGIN_TYPE, loginTypeStr);
        hideDialog();
        navigateToMain();
    }

    @Override
    public void onGetBasketListFailure(String message) {
        hideDialog();
        Toast.makeText(this, "failed to login " + message, Toast.LENGTH_SHORT).show();
    }

    protected void showDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    protected void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }


    }

    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;


    private void requestLocationPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_FINE_LOCATION);


                // MY_PERMISSION_REQUEST_READ_FINE_LOCATION is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

//    private void alertAppUpdate() {
//
//        Log.i("Service", "service started");
//        final int remoteVersionCode = CheckForUpdate.getRemoteVersionNumber(this);
//        final PreferenceUtils preferenceUtils = new PreferenceUtils(this);
//        if (AppLaunchChecker.hasStartedFromLauncher(this)) {
//            preferenceUtils.createAppVersionCode(remoteVersionCode);
//            Log.i("First time", "First time app is launched");
//        }
//        int existingVersionCode = preferenceUtils.getAppVersionCode();
//        if (remoteVersionCode > existingVersionCode) {
//        /*
//          **
//          * app is updated, alert user to update app from playstore display bottom sheet with three options Update Now, Update Later
//          * if app is updated then only save the version code in preferenceUtils
//          *
//         */
//            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this.getApplication());
//            dialogBuilder.setTitle("Update available");
//            dialogBuilder.setMessage("Do you want to update your app now?");
//            dialogBuilder.setPositiveButton("Update Now", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    CheckForUpdate.launchPlayStoreApp(getApplicationContext());
//                    Log.i("app update service", "app is needed to update");
//                    preferenceUtils.createAppVersionCode(remoteVersionCode);
//
//                }
//            });
//            dialogBuilder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//
//                }
//            });
//            dialogBuilder.show();
//
//
//        }
//    }
//

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    private void alertAppUpdate() {
        VersionChecker versionChecker = new VersionChecker();
        float remoteVersionCode = 0;
        try {
            remoteVersionCode = Float.parseFloat(versionChecker.execute().get());
            float existingVersion = CheckForUpdate.getRemoteVersionNumber(this);
            PreferenceUtils preferenceUtils = new PreferenceUtils(this);
            float existingVersionCode = Float.parseFloat(String.valueOf(preferenceUtils.getAppVersionCode()));
            if (remoteVersionCode > existingVersionCode) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this.getApplication());
                dialogBuilder.setTitle("Update available");
                dialogBuilder.setMessage("Do you want to update your app now?");
                dialogBuilder.setPositiveButton("Update Now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        CheckForUpdate.launchPlayStoreApp(getApplicationContext());
                        Log.i("app update service", "app is needed to update");
                        // preferenceUtils.createAppVersionCode(remoteVersionCode);

                    }
                });
                dialogBuilder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                dialogBuilder.show();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /*polling the server for version code */
    public class VersionChecker extends AsyncTask<String, String, String> {
        String newVersion = "0.0";

        @Override
        protected String doInBackground(String... params) {
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + activityContext.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;
        }
    }
}