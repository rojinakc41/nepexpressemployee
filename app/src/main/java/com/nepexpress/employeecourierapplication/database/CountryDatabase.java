package com.nepexpress.employeecourierapplication.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.nepexpress.employeecourierapplication.Country;

/**
 * Created by Jeevan Acharya on 3/29/2019.
 */
@Database(entities = {Country.class}, version = 1, exportSchema = false)
public abstract class CountryDatabase extends RoomDatabase {
    private static CountryDatabase INSTANCE;

    // public abstract CourierDAO courierDAO();

    public abstract CountryDAO countryDAO();

    private static final Migration MIGRATION_2_1 = new Migration(2, 1) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            //       database.execSQL("CREATE TABLE IF NOT EXISTS `courier` (`DId` TEXT NOT NULL, `orderId` TEXT, `name` TEXT, `address` TEXT, `amount` TEXT, `contact` TEXT, `quantity` TEXT, `status` TEXT, `staff` TEXT, `parcel` TEXT, `date` TEXT, `comment` TEXT, `sender` TEXT, `senderLatLng` TEXT, `receiverLatLng` TEXT, `isSavedStatus` INTEGER, PRIMARY KEY(`DId`))");
            database.execSQL("CREATE TABLE IF NOT EXISTS `Country` (`countryName` TEXT, `countryCode` TEXT, `countryId` TEXT NOT NULL, PRIMARY KEY(`countryId`))");
        }
    };
    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            //     database.execSQL("CREATE TABLE IF NOT EXISTS `courier` (`DId` TEXT NOT NULL, `orderId` TEXT, `name` TEXT, `address` TEXT, `amount` TEXT, `contact` TEXT, `quantity` TEXT, `status` TEXT, `staff` TEXT, `parcel` TEXT, `date` TEXT, `comment` TEXT, `sender` TEXT, `senderLatLng` TEXT, `receiverLatLng` TEXT, `isSavedStatus` INTEGER, PRIMARY KEY(`DId`))");
            database.execSQL("CREATE TABLE IF NOT EXISTS `Country` (`countryName` TEXT, `countryCode` TEXT, `countryId` TEXT NOT NULL, PRIMARY KEY(`countryId`))");
        }
    };

    public static CountryDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), CountryDatabase.class, "courier-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .addMigrations(MIGRATION_2_1, MIGRATION_2_3)
                            .fallbackToDestructiveMigration()
                            .build();
        }
        return INSTANCE;
    }
}

