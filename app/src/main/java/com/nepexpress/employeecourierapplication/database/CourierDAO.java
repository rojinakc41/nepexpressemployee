package com.nepexpress.employeecourierapplication.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.nepexpress.employeecourierapplication.todaysBasket.Courier;

import java.util.List;

/**
 * Created by rojin on 1/18/2018.
 */


@Dao
public interface CourierDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
        //Replace thyo pahila  changed due to download nature on pick
    void insertAllCourier(List<Courier> courierList);

    @Update
    void update(Courier courier);

    @Query("SELECT * FROM courier")
    List<Courier> getAllCourier();


    @Query("DELETE FROM courier")
    void deleteAllCourier();

    @Delete
    void deleteIndividualCourier(Courier courier);


    @Query(value = "SELECT * FROM courier WHERE isSavedStatus = :status")
    List<Courier> getCourierYetToBeParceled(Boolean status);

    @Query("UPDATE courier SET isSavedStatus = :savedStatus, Status=:courierStatus, Date=:courierDate, Comment=:comment  WHERE DId = :id")
    void updateCourier(Boolean savedStatus, String id, String courierStatus, String comment, String courierDate);

    @Query("SELECT COUNT(Status) FROM courier WHERE Status Like 'Delivered' ")
    int getNumberOfDeliveredCourier();

    @Query("SELECT COUNT(Status) FROM courier WHERE Status Like 'Cancelled' or Status LIKe 'Rejected'")
    int getNumberOfCanceledAndRejectedCourier();

    @Query("SELECT COUNT(Status) FROM courier WHERE Status Like 'Held'")
    int getNumberOfPendingCourier();

    @Query("Select * FROM courier where not Status like 'Dispatched'")
    List<Courier> getCourierListOrderFulfilled();


}
