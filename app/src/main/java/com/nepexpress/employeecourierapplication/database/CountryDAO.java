package com.nepexpress.employeecourierapplication.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.nepexpress.employeecourierapplication.Country;

import java.util.List;

@Dao
public interface CountryDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllCountry(List<Country> countryList);


    @Query("SELECT * FROM country")
    List<Country> getAllCountries();


}
