package com.nepexpress.employeecourierapplication.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.nepexpress.employeecourierapplication.mainMap.GetPackagesModel;

import java.util.List;

/**
 * Created by Jeevan Acharya on 3/9/2019.
 */
@Dao
public interface GetPackageDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllCourier(List<GetPackagesModel> courierList);

    @Query("SELECT * FROM package")
    List<GetPackagesModel> getAllPacakges();


    @Query("SELECT COUNT(Status) FROM package WHERE Status Like 'Delivered' ")
    int getNumberOfDeliveredPackage();

    @Query("SELECT COUNT(Status) FROM package WHERE Status Like 'Cancelled' or Status LIKe 'Rejected'")
    int getNumberOfCanceledAndRejectedPackage();


    @Query("UPDATE package SET  Status=:status  WHERE DId = :did")
    void updatePackage(int did, String status);

    @Query("DELETE FROM package")
    void deleteAllPackages();

}
