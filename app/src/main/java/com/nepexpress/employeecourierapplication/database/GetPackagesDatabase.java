package com.nepexpress.employeecourierapplication.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.nepexpress.employeecourierapplication.mainMap.GetPackagesModel;

/**
 * Created by Jeevan Acharya on 3/9/2019.
 */

@Database(entities = GetPackagesModel.class, version = 2, exportSchema = false)
public abstract class GetPackagesDatabase extends RoomDatabase {

    public abstract GetPackageDao getPackageDao();

    private static GetPackagesDatabase INSTANCE;

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `package` (`dId` INTEGER NOT NULL, `orderId` TEXT, `name` TEXT, `address` TEXT, `amount` INTEGER, `contact` TEXT, `quantity` TEXT, `status` TEXT, `staff` TEXT, `parcel` TEXT, `date` TEXT, `comment` TEXT, `sender` TEXT, `senderLatang` TEXT, `receiverLatang` TEXT, `weight` INTEGER, PRIMARY KEY(`dId`))");
        }
    };
    private static final Migration MIGRATION_1_3 = new Migration(1, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `package` (`dId` INTEGER NOT NULL, `orderId` TEXT, `name` TEXT, `address` TEXT, `amount` INTEGER, `contact` TEXT, `quantity` TEXT, `status` TEXT, `staff` TEXT, `parcel` TEXT, `date` TEXT, `comment` TEXT, `sender` TEXT, `senderLatang` TEXT, `receiverLatang` TEXT, `weight` INTEGER, PRIMARY KEY(`dId`))");
        }
    };

    public static GetPackagesDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (GetPackagesDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), GetPackagesDatabase.class,
                            "courier-database")
                            .allowMainThreadQueries()
                            .addMigrations(MIGRATION_1_2,MIGRATION_1_3)
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
