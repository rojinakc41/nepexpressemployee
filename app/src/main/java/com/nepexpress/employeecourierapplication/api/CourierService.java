package com.nepexpress.employeecourierapplication.api;


import com.nepexpress.employeecourierapplication.AgentRate;
import com.nepexpress.employeecourierapplication.Country;
import com.nepexpress.employeecourierapplication.createAgent.CreateAgent;
import com.nepexpress.employeecourierapplication.createAgent.CreateAgentResponse;
import com.nepexpress.employeecourierapplication.mainMap.GetPackagesModel;
import com.nepexpress.employeecourierapplication.mainMap.UpdateBasketModel;
import com.nepexpress.employeecourierapplication.oneSignalNotification.NotiDeviceModel;
import com.nepexpress.employeecourierapplication.requestOrder.RequestOrder;
import com.nepexpress.employeecourierapplication.requestOrder.RequestOrderResponse;
import com.nepexpress.employeecourierapplication.routePoints.RoutePoints;
import com.nepexpress.employeecourierapplication.todaysBasket.Activity;
import com.nepexpress.employeecourierapplication.todaysBasket.Courier;
import com.nepexpress.employeecourierapplication.todaysBasket.SaveActivityResponse;
import com.nepexpress.employeecourierapplication.trackParcel.TrackParcel;
import com.nepexpress.employeecourierapplication.uploadBasket.CourierToUpload;
import com.nepexpress.employeecourierapplication.uploadBasket.UploadBasketResponse;
import com.nepexpress.employeecourierapplication.util.AppText;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by rojin on 9/20/2017.
 */

public interface CourierService {

    @POST()
    Call<List<Courier>> downloadCourierList(@Header("Authorization") String credential, @Url String url);

    @POST()
    Call<UploadBasketResponse> sendCouriers(@Header("Authorization") String credential, @Url String url, @Body List<CourierToUpload> courierUploadList);

    @GET()
    Call<RoutePoints> getRoutePoints(@Url String url);

    @POST(AppText.URL_REQUEST_ORDER)
    Call<RequestOrderResponse> sendRequestOrder(@Header("Authorization") String credential, @Body RequestOrder requestOrder);

    @POST()
    Call<List<TrackParcel>> trackParcel(@Header("Authorization") String credentail, @Url String url, @Query("OrderId") String orderId);

    @GET(AppText.URL_COUNTRY_LIST)
    Call<List<Country>> getCountryList(@Header("Authorization") String credential);

    @POST(AppText.URL_AGENT_RATE)
    Call<AgentRate> getAgentRate(@Header("Authorization") String credential,
                                 @Query("CountryCode") String countryCode,
                                 @Query("TotalWeight") String totalWeight);


    @POST(AppText.URL_CREATE_AGENT)
    Call<CreateAgentResponse> createAgent(@Header("Authorization") String credential,
                                          @Body CreateAgent createAgent);


    @POST(AppText.URL_SAVE_ACTIVITY)
    Call<SaveActivityResponse> saveActivity(@Header("Authorization") String credential, @Body Activity activity);


    @GET("Employee/GetPackages")
    Call<List<GetPackagesModel>> getTestPackages(@Header("Authorization") String credential);

    @POST("Employee/UpdateBasket")
    Call<UpdateBasketModel.UpdateBasketMessage> updatebasket(@Header("Authorization") String credential, @Body List<UpdateBasketModel> model);

    @FormUrlEncoded
    @POST("Employee/SaveNotification")
    Call<NotiDeviceModel> postNotificationDeviceId(@Header("Authorization") String credential,
                                                   @Field("StaffId") String staffId,
                                                   @Field("Location") String location,
                                                   @Field("UserType") String userType,
                                                   @Field("DeviceId") String deviceId,
                                                   @Field("MessageId") String messageId,
                                                   @Field("Timestamp") String time);

    @FormUrlEncoded
    @POST("Employee/SaveCurrentLocation")
    Call<ResponseBody> postEmployeeLocation(@Header("Authorization") String credential,
                                            @Field("Staff") String Staff,
                                            @Field("DId") Integer DId,
                                            @Field("OrderId") String OrderId,
                                            @Field("StaffId") String StaffId,
                                            @Field("Latang") String Latang,
                                            @Field("Timestamp") String time,
                                            @Field("IsActive") Boolean isActive);


}
