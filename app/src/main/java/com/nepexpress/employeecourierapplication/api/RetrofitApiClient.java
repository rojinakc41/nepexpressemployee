package com.nepexpress.employeecourierapplication.api;

import android.util.Log;


import com.nepexpress.employeecourierapplication.util.AppText;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by rojin on 9/20/2017.
 */

public class RetrofitApiClient {

    private static final String TAG=RetrofitApiClient.class.getSimpleName();

    public static Retrofit getClient() {
        HttpLoggingInterceptor interceptor=new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
     httpClient.connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100,TimeUnit.SECONDS).build();
        httpClient.addInterceptor(interceptor);
                OkHttpClient cl = httpClient.build();
                 Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppText.BASE_URL)

                .addConverterFactory(GsonConverterFactory.create())
                         .client(cl)
                .build();

            Log.d(TAG,"inside retrofit client");


        return retrofit;
    }



    public static Retrofit getGoogleClient() {

        Log.d(TAG,"inside getting goole client");

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add your other interceptors …
        // add logging as last interceptor
        httpClient.addInterceptor(logging);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppText.BASE_URL_FOR_GOOGLE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();


        return retrofit;
    }
    public static CourierService getGoogleService(){
        return RetrofitApiClient.getGoogleClient().create(CourierService.class);
    }

    public static CourierService getCourierService() {
        return RetrofitApiClient.getClient().create(CourierService.class);
    }





}
