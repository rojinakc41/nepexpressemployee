package com.nepexpress.employeecourierapplication.mainMap;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.LocationManager;

import okhttp3.Credentials;

import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.RequestResult;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Info;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;
import com.nepexpress.employeecourierapplication.base.BaseActivity;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.showMap.MapsActivity;
import com.nepexpress.employeecourierapplication.todaysBasket.Courier;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketContract;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketDetailActivity;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketFragment;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketPresenter;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetPackageDetailActivity extends BaseActivity implements TodaysBasketContract.View
        , TodaysBasketFragment.OnTodayBasketFragmentInteractionListener {

    GetPackagesModel model = new GetPackagesModel();
    TextView orderIdTv, orderNameTv, addressTv, phoneTv, amountTv, quantity, date, parcel, senderName;
    EditText commentEt;
    Button saveCourier, navigateToMap; ///addImage;
    Spinner statusCourier;
    String courierState;
    SharedPrefUtil sharedPrefUtil;
    //  GetPackagesDatabase mDatabase;
    CourierDatabse mDatabase;
    List<UpdateBasketModel> modelList = new ArrayList<>();
    CourierService service;
    String credentials;
    boolean isSentToServer = false;
    String result, username, password;
    TodaysBasketPresenter todaysBasketPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getSupportActionBar() != null;
        sharedPrefUtil = new SharedPrefUtil(this);

        todaysBasketPresenter = new TodaysBasketPresenter(this);

        mDatabase = CourierDatabse.getAppDatabase(this);
        getSupportActionBar().setTitle("Package Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_get_package_detail);
        result = sharedPrefUtil.getPackageModel();
        if (result == null) {
            onBackPressed();
        }
        Gson gson = new Gson();
        model = gson.fromJson(result, GetPackagesModel.class);
        service = RetrofitApiClient.getCourierService();
        username = sharedPrefUtil.getStrings(AppText.KEY_USER_NAME);
        password = sharedPrefUtil.getStrings(AppText.KEY_USER_PASSWORD);
        credentials = Credentials.basic(username, password);

        defineView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void defineView() {
        orderIdTv = findViewById(R.id.order_id_tv);
        parcel = findViewById(R.id.order_parcel);
        orderNameTv = findViewById(R.id.order_name_tv);
        addressTv = findViewById(R.id.order_address_tv);
        phoneTv = findViewById(R.id.show_phone_no);
        amountTv = findViewById(R.id.order_amount);
        quantity = findViewById(R.id.order_quantity);
        saveCourier = findViewById(R.id.save_courier);
        commentEt = findViewById(R.id.show_comment_et);
        date = findViewById(R.id.show_date);
        navigateToMap = findViewById(R.id.navigate_to_latlng);
        statusCourier = findViewById(R.id.show_status_spinner);
        senderName = findViewById(R.id.sender_name);
/*
        if (sharedPrefUtil.getPickMode() != null && sharedPrefUtil.getPickMode() == true) {
            statusCourier.setSelection(1);
            courierState = statusCourier.getSelectedItem().toString();
        }
*/
        bindView();
        onclickListener();
    }

    private void bindView() {
        orderIdTv.setText("OrderId: " + model.getOrderId());
        orderNameTv.setText("Name: " + model.getName());
        addressTv.setText("Address: " + model.getAddress());
        phoneTv.setText("Contact: " + model.getContact());
        date.setText("Date: " + model.getDate());
        amountTv.setText("Amount: " + model.getAmount());
        quantity.setText("Quantity: " + model.getQuantity());

        parcel.setText("Parcel: " + model.getParcel());
        senderName.setText("" + model.getSender());

        if (model.getComment() != null) {
            commentEt.setText(android.text.Html.fromHtml(model.getComment()));
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    private void onclickListener() {
        commentEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });
        statusCourier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    courierState = statusCourier.getSelectedItem().toString();
                }
                if (i == 1) {
                    // if (sharedPrefUtil.getPickMode() == false) {
                    //setModelForReleaseAndPick(i);
                    showDialog(" Please Wait.......");
                    if (model.getSenderLatang() != null && model.getReceiverLatang() != null) {
                        getDestinationInfoForPickAndRelease(Double.parseDouble(model.getSenderLatang().split(",")[0]), Double.parseDouble(model.getSenderLatang().split(",")[1]),
                                Double.parseDouble(model.getReceiverLatang().split(",")[0]), Double.parseDouble(model.getReceiverLatang().split(",")[1]), i);

                    } else {
                        setModelForReleaseAndPick(i, 0);
                    }
                    //}
                    saveCourier.setVisibility(View.GONE);

                } else if (i == 2) {
                    setModelForReleaseAndPick(i, 0.0);
                } else {
                    saveCourier.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        navigateToMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
                boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
                if (!enabled) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 1);
                } else {
                    Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                    if (!TextUtils.isEmpty(model.getReceiverLatang()))
                        intent.putExtra(MapsActivity.ARG_RECEIVER_LATLNG, model.getReceiverLatang());

                    if (!TextUtils.isEmpty(model.getSenderLatang()))
                        intent.putExtra(MapsActivity.ARG_SENDER_LATLNG, model.getSenderLatang());
                    startActivity(intent);
                }
            }
        });

        saveCourier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = commentEt.getText().toString();
                if (TextUtils.isEmpty(courierState))
                    Toast.makeText(GetPackageDetailActivity.this, "Please select status of courier", Toast.LENGTH_SHORT).show();

                else {
                    showDialog("Sending activity to server");
                    if (model.getSenderLatang() != null && model.getReceiverLatang() != null) {
                        getDestinationInfo(Double.parseDouble(model.getSenderLatang().split(",")[0]), Double.parseDouble(model.getSenderLatang().split(",")[1]),
                                Double.parseDouble(model.getReceiverLatang().split(",")[0]), Double.parseDouble(model.getReceiverLatang().split(",")[1]), comment);

                    } else {
                        setModel(comment, 0);
                    }
                }
            }
        });
    }

    private void setModel(String comment, double distance) {
        UpdateBasketModel activity = new UpdateBasketModel();
        activity.setComment(comment);
        activity.setStatus(courierState);
        activity.setDeliveryDate(getDateTime());
        activity.setDID(model.getDId());
        activity.setOrderId(model.getOrderId());
        activity.setStaff(sharedPrefUtil.getStrings(AppText.KEY_USER_NAME));
        activity.setDistance(distance);
//        if (model.getSenderLatang() != null && model.getReceiverLatang() != null) {
//            double dist = getDestinationInfo(Double.parseDouble(model.getSenderLatang().split(",")[0]), Double.parseDouble(model.getSenderLatang().split(",")[1]),
//                    Double.parseDouble(model.getReceiverLatang().split(",")[0]), Double.parseDouble(model.getReceiverLatang().split(",")[1]));
//            activity.setDistance(dist);
//        } else {
//            activity.setDistance(0.0);
//        }
        updateNewBasket(activity);
    }

    private void updateNewBasket(final UpdateBasketModel updateBasketModel) {

        modelList.clear();
        modelList.add(updateBasketModel);
        service.updatebasket(credentials, modelList).enqueue(new Callback<UpdateBasketModel.UpdateBasketMessage>() {
            @Override
            public void onResponse(Call<UpdateBasketModel.UpdateBasketMessage> call, Response<UpdateBasketModel.UpdateBasketMessage> response) {
                if (response.isSuccessful()) {
                    hideDialog();
                    sharedPrefUtil.insertPickMode(false);
                    sharedPrefUtil.insertGepPackageModelData(null);
                    model.setStatus(updateBasketModel.getStatus());
                    mDatabase.getPackageDao().updatePackage(model.getDId(), updateBasketModel.getStatus());
                    onBackPressed();
                    Toast.makeText(GetPackageDetailActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    hideDialog();
                    Toast.makeText(GetPackageDetailActivity.this, "Error sending to server!! Try again", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<UpdateBasketModel.UpdateBasketMessage> call, Throwable t) {
                hideDialog();
                Toast.makeText(GetPackageDetailActivity.this, "Error sending to server!! Try again", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private String getDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        String date = sdf.format(new Date());
        return date.split(" ")[0] + "T" + date.split(" ")[1];
    }

    private void setModelForReleaseAndPick(int i, double distance) {
        showDialog(" Please Wait.......");
        UpdateBasketModel modelUpdate = new UpdateBasketModel();
        modelUpdate.setDistance(distance);
        modelUpdate.setDeliveryDate(getDateTime());
        if (courierState.equalsIgnoreCase(getResources().getString(R.string.pick_mode))) {
            modelUpdate.setStaff(sharedPrefUtil.getStrings(AppText.KEY_USER_NAME));
            modelUpdate.setStatus("Dispatched");
        } else {
            modelUpdate.setStaff(null);
            modelUpdate.setStatus(null);
        }
        modelUpdate.setOrderId(model.getOrderId());
        modelUpdate.setDID(model.getDId());
        modelUpdate.setComment(commentEt.getText().toString());
        requestOnReleaseAndPick(modelUpdate, i);
    }


    private void requestOnReleaseAndPick(UpdateBasketModel updateBasketModel, final int i) {
        modelList.clear();
        modelList.add(updateBasketModel);
        service.updatebasket(credentials, modelList).enqueue(new Callback<UpdateBasketModel.UpdateBasketMessage>() {
            @Override
            public void onResponse(Call<UpdateBasketModel.UpdateBasketMessage> call, Response<UpdateBasketModel.UpdateBasketMessage> response) {
                if (response.isSuccessful()) {
                    if (i == 1) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        sharedPrefUtil.insertPickMode(true);
                        sharedPrefUtil.insertGepPackageModelData(result);
                        saveCourier.setVisibility(View.GONE);

                    } else {
                        sharedPrefUtil.insertPickMode(false);
                        sharedPrefUtil.insertGepPackageModelData(null);
                        getSupportActionBar().setTitle("Package Detail");
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        saveCourier.setVisibility(View.GONE);

                    }
                    String message = response.body().getMessage();
                    if (message.trim().equalsIgnoreCase(getResources().getString(R.string.already_picked)) ||
                            message.trim().equalsIgnoreCase(getResources().getString(R.string.unauthrorized_to_release))) {
                        finish();
                    } else {
                        if (modelList.get(0).getStatus() != null) {
                            showDialog("Loading your courier");
                            todaysBasketPresenter.getTodayBasket(username, password, AppText.URL_DOWNLOAD_BASKET);
                        }
                    }
                    Toast.makeText(GetPackageDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                    hideDialog();
                } else {
                    Toast.makeText(GetPackageDetailActivity.this, " Try again", Toast.LENGTH_SHORT).show();
                    hideDialog();
                }
            }

            @Override
            public void onFailure(Call<UpdateBasketModel.UpdateBasketMessage> call, Throwable t) {
                Toast.makeText(GetPackageDetailActivity.this, "Try again", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_todays_basket_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_make_phone_call) {
            dialNumber();
            return true;
        } else if (id == R.id.action_send_message) {
            sendMessage();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendMessage() {

        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", model.getContact(), null)));
    }


    public void dialNumber() {
        String phoneNumber = String.format("tel: %s",
                model.getContact());
        // Create the intent.
        Intent dialIntent = new Intent(Intent.ACTION_DIAL);
        // Set the data for the intent as the phone number.
        dialIntent.setData(Uri.parse(phoneNumber));
        // If package resolves to an app, send intent.
        if (dialIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(dialIntent);
        } else {
            Log.e("TAG", "Can't resolve app for ACTION_DIAL Intent.");
        }
    }

    private void getDestinationInfo(double lat1, double lang1, double lat2, double lang2, final String comment) {
        String serverKey = getResources().getString(R.string.google_maps_key); // Api Key For Google Direction API \\
        final LatLng origin = new LatLng(lat1, lang1);
        final LatLng destination = new LatLng(lat2, lang2);
        //-------------Using AK Exorcist Google Direction Library---------------\\
        GoogleDirection.withServerKey(serverKey)
                .from(origin)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        String status = direction.getStatus();
                        if (status.equals(RequestResult.OK)) {
                            Route route = direction.getRouteList().get(0);
                            Leg leg = route.getLegList().get(0);
                            Info distanceInfo = leg.getDistance();
                            Info durationInfo = leg.getDuration();
                            String distance = distanceInfo.getText();
                            String duration = durationInfo.getText();
                            Log.e("distance", distance);
                            setModel(comment, Double.parseDouble(distance.split(" ")[0]));
                            //------------Displaying Distance and Time-----------------\\
                            //showingDistanceTime(distance, duration); // Showing distance and time to the user in the UI \\
//                            String message = "Total Distance is " + distance + " and Estimated Time is " + duration;
//                            StaticMethods.customSnackBar(consumerHomeActivity.parentLayout, message,
//                                    getResources().getColor(R.color.colorPrimary),
//                                    getResources().getColor(R.color.colorWhite), 3000);


                        } else if (status.equals(RequestResult.NOT_FOUND)) {
                            setModel(comment, 0);
                            //Toast.makeText(this, "No routes exist", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                        setModel(comment, 0);
                    }
                });
    }

    private void getDestinationInfoForPickAndRelease(double lat1, double lang1, double lat2, double lang2, final int i) {
        String serverKey = getResources().getString(R.string.google_maps_key); // Api Key For Google Direction API \\
        final LatLng origin = new LatLng(lat1, lang1);
        final LatLng destination = new LatLng(lat2, lang2);
        //-------------Using AK Exorcist Google Direction Library---------------\\
        GoogleDirection.withServerKey(serverKey)
                .from(origin)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        String status = direction.getStatus();
                        if (status.equals(RequestResult.OK)) {
                            Route route = direction.getRouteList().get(0);
                            Leg leg = route.getLegList().get(0);
                            Info distanceInfo = leg.getDistance();
                            Info durationInfo = leg.getDuration();
                            String distance = distanceInfo.getText();
                            String duration = durationInfo.getText();
                            Log.e("distance", distance);
                            setModelForReleaseAndPick(i, Double.parseDouble(distance.split(" ")[0]));
                            //------------Displaying Distance and Time-----------------\\
                            //showingDistanceTime(distance, duration); // Showing distance and time to the user in the UI \\
//                            String message = "Total Distance is " + distance + " and Estimated Time is " + duration;
//                            StaticMethods.customSnackBar(consumerHomeActivity.parentLayout, message,
//                                    getResources().getColor(R.color.colorPrimary),
//                                    getResources().getColor(R.color.colorWhite), 3000);


                        } else if (status.equals(RequestResult.NOT_FOUND)) {
                            setModelForReleaseAndPick(i, 0);
                            //Toast.makeText(this, "No routes exist", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                        setModelForReleaseAndPick(i, 0);
                    }
                });

    }

    @Override
    public void onGetBasketListSuccess(List<Courier> courierList) {
      //  mDatabase.courierDAO().deleteAllCourier();
        hideDialog();
        mDatabase.courierDAO().insertAllCourier(courierList);
        // todayBasketFragment();

    }

    @Override
    public void onGetBasketListFailure(String message) {
        hideDialog();
//        Toast.makeText(this, "Couldn't get any response from the server", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onCourierClicked(Courier courier) {
        Intent intent = new Intent(GetPackageDetailActivity.this, TodaysBasketDetailActivity.class);
        intent.putExtra(TodaysBasketDetailActivity.KEY_PARCEL_DETAIL, courier);
        startActivity(intent);

    }
}
/*private void sendSMS() {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
      {
         String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); // Need to change the build to API 19

         Intent sendIntent = new Intent(Intent.ACTION_SEND);
         sendIntent.setType("text/plain");
         sendIntent.putExtra(Intent.EXTRA_TEXT, "text");

         if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
         // any app that support this intent.
         {
            sendIntent.setPackage(defaultSmsPackageName);
         }
         startActivity(sendIntent);

      }
      else // For early versions, do what worked for you before.
      {
         Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
         smsIntent.setType("vnd.android-dir/mms-sms");
         smsIntent.putExtra("address","phoneNumber");
         smsIntent.putExtra("sms_body","message");
         startActivity(smsIntent);
      }
   }*/