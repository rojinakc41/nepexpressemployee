package com.nepexpress.employeecourierapplication.mainMap;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.routePoints.GetRoutePointContract;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainDashBoardFragment extends Fragment implements LocationListener, OnMapReadyCallback, GetRoutePointContract.View, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final String MY_LOCATION = "My Location";
    public static final String PARCEL = "Parcel";

    private String mParam1;
    private String mParam2;

    Context mContext;
    GoogleApiClient mLocationClient;

    GoogleMap mMap;
    SupportMapFragment mapFragment;
    PolylineOptions polylineOptions;
    LocationManager locationManager;
   // GetPackagesDatabase mDatabase;
    CourierDatabse mDatabase;

    public MainDashBoardFragment() {
    }

    public static MainDashBoardFragment newInstance(String param1, String param2) {
        MainDashBoardFragment fragment = new MainDashBoardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mDatabase = CourierDatabse.getAppDatabase(mContext);
        locationManager = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 400, 1000, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }

                mMap.setMyLocationEnabled(true);


            }
        });
        mLocationClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks((new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                }))
                .addOnConnectionFailedListener((new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                }))
                .build();
        mLocationClient.connect();
        getRemotePackages();
        fetchRemoteDataInTimeIntervel();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_main_dash_board, container, false);
        setupViews(mView);
        return mView;
    }

    private void setupViews(View mView) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

    }


    private void getRemotePackages() {
        CourierService service = RetrofitApiClient.getClient().create(CourierService.class);
        SharedPrefUtil prefUtil = new SharedPrefUtil(mContext);
        String userNameStr = prefUtil.getStrings(AppText.KEY_USER_NAME);
        String passwordStr = prefUtil.getStrings(AppText.KEY_USER_PASSWORD);

        String credentials = Credentials.basic(userNameStr, passwordStr);

        service.getTestPackages(credentials).enqueue(new Callback<List<GetPackagesModel>>() {
            @Override
            public void onResponse(Call<List<GetPackagesModel>> call, Response<List<GetPackagesModel>> response) {
                if (response.isSuccessful()) {
                    List<GetPackagesModel> results = response.body();
                    mDatabase.getPackageDao().insertAllCourier(results);
                    for (GetPackagesModel model : results) {
                        // if (model.getSenderLatang() == null) return;
                        if (model.getSenderLatang() != null) {
                            try {
                                String name = PARCEL;
                                if (model.getParcel() != null) {
                                    name = model.getParcel();
                                }
                                addMarker(model.getSenderLatang().trim(), name, model);

                            } catch (Exception e) {

                                String value = model.getSenderLatang().split(":")[1].trim();
                                String latLang = value.substring(1, value.length() - 1);
                                String name = PARCEL;
                                if (model.getParcel() != null) {
                                    name = model.getParcel();
                                }
                                addMarker(latLang, name, model);

                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<GetPackagesModel>> call, Throwable t) {

            }
        });
    }

    private void addMarker(String latLang, String name, GetPackagesModel model) {
        try {
            MarkerOptions markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(Double.parseDouble(latLang.split(",")[0]), Double.parseDouble(latLang.split(",")[1]));
            markerOptions.position(latLng);

            //mMap.clear();
            markerOptions.title(name);

            // markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)); //jeevan comment
            markerOptions.getPosition();
            //markerOptions.icon(BitmapDescriptorFactory.fromBitmap(writeOnDrawable(R.drawable.ic_marker, model.getParcel()).getBitmap()));

            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createCustomMarker(mContext, R.drawable.ic_marker, model.getParcel())));
            Marker marker = mMap.addMarker(markerOptions);
            // marker.setIcon(BitmapDescriptorFactory.fromBitmap(writeOnDrawable(R.drawable.ic_marker, model.getParcel()).getBitmap()));

            // marker.setIcon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon(model.getParcel()))); //jeevan
            //marker.showInfoWindow();
            Gson mGson = new Gson();
            String argItem = mGson.toJson(model, GetPackagesModel.class);
            marker.setTag(argItem);
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if (!marker.getTitle().equalsIgnoreCase(MY_LOCATION)) {
                        SharedPrefUtil prefUtil = new SharedPrefUtil(mContext);
                        prefUtil.insertGepPackageModelData(marker.getTag().toString());
                        Intent intent = new Intent(mContext, GetPackageDetailActivity.class);
                        mContext.startActivity(intent);
                    }
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*
    private MarkerOptions createMarker(LatLng point, String title) {
        MarkerOptions marker = new MarkerOptions();
        marker.position(point);
        int px = mContext.getResources().getDimensionPixelSize(R.dimen.map_marker_diameter);
        View markerView = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_marker, null);
        markerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        markerView.layout(0, 0, px, px);
        markerView.buildDrawingCache();
        TextView bedNumberTextView = (TextView) markerView.findViewById(R.id.bed_num_text_view);
        Bitmap mDotMarkerBitmap = Bitmap.createBitmap(px, px, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(mDotMarkerBitmap);
        bedNumberTextView.setText(title);
        markerView.draw(canvas);
        marker.icon(BitmapDescriptorFactory.fromBitmap(mDotMarkerBitmap));
        return marker;
    }
*/

    /*private BitmapDrawable writeOnDrawable(int drawableId, String text) {

        Bitmap bm = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_marker).copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint(TextPaint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK);
        paint.setTextSize(30);
        paint.setTextAlign(Paint.Align.CENTER);
        Canvas canvas = new Canvas(bm);

        // Change the position of text here
        canvas.drawText("Jeevan Acharya is Good", bm.getWidth() / 2 //x position
                , bm.getHeight() / 2  // y position
                , paint);
        return new BitmapDrawable(mContext.getResources(), bm);
    }*/
    Bitmap bitmap;
    Canvas canvas;

    public Bitmap createCustomMarker(Context context, @DrawableRes int resource, String _name) {
        try {

            View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_marker, null);

            TextView txt_name = (TextView) marker.findViewById(R.id.name);
            txt_name.setText(_name);

            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            marker.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
            marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
            marker.buildDrawingCache();
            bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            canvas = new Canvas(bitmap);
            marker.draw(canvas);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void getRoutePointSuccess(ArrayList<LatLng> lngArrayList, String estimatedTime, String totalDistance) {

    }

    @Override
    public void getRoutePointFailure(String message) {

    }

    @Override
    public void onLocationChanged(Location location) {
        try {

            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
            mMap.animateCamera(cameraUpdate);
            locationManager.removeUpdates(this);
            MarkerOptions markerOptions = new MarkerOptions();

            // Setting the position for the marker
            markerOptions.position(latLng);
            //mMap.clear();
            markerOptions.title(MY_LOCATION);
            try {
                Bitmap markerBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_bike, null);
                markerBitmap = scaleBitmap(markerBitmap, 100, 100);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerBitmap));

            } catch (Exception e) {
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }
            markerOptions.getPosition();

            mMap.addMarker(markerOptions);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHeight / (float) bitmap.getHeight();
        float pivotX = 0;
        float pivotY = 0;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    //int count = 0;

    private void fetchRemoteDataInTimeIntervel() {
        getRemotePackages();
        SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(mContext);
        if (sharedPrefUtil.getPackageCallStatus() == false) {
            sharedPrefUtil.insertPackageThreadStatus(true);
            final Handler handler = new Handler();
            Timer timer = new Timer();
            TimerTask doAsynchronousTask = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        public void run() {
                            try {
                                //  Toast.makeText(mContext, "hitted server", Toast.LENGTH_SHORT).show();
                                getRemotePackages();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            };
            timer.schedule(doAsynchronousTask, 0, 60000); //execute in every 10 minutes
            //timer.schedule(doAsynchronousTask, 0, 5000);
        }
    }
}
