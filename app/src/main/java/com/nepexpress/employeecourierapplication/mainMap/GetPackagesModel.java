package com.nepexpress.employeecourierapplication.mainMap;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jeevan Acharya on 3/7/2019.
 */

@Entity(tableName = "package")
public class GetPackagesModel {

    @NonNull
    @PrimaryKey
    @ColumnInfo
    @SerializedName("DId")
    @Expose
    private Integer dId;

    @ColumnInfo
    @SerializedName("OrderId")
    @Expose
    private String orderId;

    @ColumnInfo
    @SerializedName("Name")
    @Expose
    private String name;

    @ColumnInfo
    @SerializedName("Address")
    @Expose
    private String address;

    @ColumnInfo
    @SerializedName("Amount")
    @Expose
    private Integer amount;

    @ColumnInfo
    @SerializedName("Contact")
    @Expose
    private String contact;

    @ColumnInfo
    @SerializedName("Quantity")
    @Expose
    private String quantity;

    @ColumnInfo
    @SerializedName("Status")
    @Expose
    private String status;

    @ColumnInfo
    @SerializedName("Staff")
    @Expose
    private String staff;

    @ColumnInfo
    @SerializedName("Parcel")
    @Expose
    private String parcel;

    @ColumnInfo
    @SerializedName("Date")
    @Expose
    private String date;

    @ColumnInfo
    @SerializedName("Comment")
    @Expose
    private String comment;

    @ColumnInfo
    @SerializedName("Sender")
    @Expose
    private String sender;

    @ColumnInfo
    @SerializedName("SenderLatang")
    @Expose
    private String senderLatang;

    @ColumnInfo
    @SerializedName("ReceiverLatang")
    @Expose
    private String receiverLatang;

    @ColumnInfo
    @SerializedName("Weight")
    @Expose
    private Integer weight;

    public Integer getDId() {
        return dId;
    }

    public void setDId(Integer dId) {
        this.dId = dId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getParcel() {
        return parcel;
    }

    public void setParcel(String parcel) {
        this.parcel = parcel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderLatang() {
        return senderLatang;
    }

    public void setSenderLatang(String senderLatang) {
        this.senderLatang = senderLatang;
    }

    public String getReceiverLatang() {
        return receiverLatang;
    }

    public void setReceiverLatang(String receiverLatang) {
        this.receiverLatang = receiverLatang;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

}
