package com.nepexpress.employeecourierapplication.mainMap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jeevan Acharya on 3/9/2019.
 */

public class UpdateBasketModel {

    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("DeliveryDate")
    @Expose
    private String deliveryDate;
    @SerializedName("DID")
    @Expose
    private Integer dID;
    @SerializedName("OrderId")
    @Expose
    private String orderId;
    @SerializedName("Staff")
    @Expose
    private String staff;
    @SerializedName("Distance")
    @Expose
    private Double distance;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getDID() {
        return dID;
    }

    public void setDID(Integer dID) {
        this.dID = dID;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public class UpdateBasketMessage {

        @SerializedName("Result")
        @Expose
        private Boolean result;
        @SerializedName("Message")
        @Expose
        private String message;

        public Boolean getResult() {
            return result;
        }

        public void setResult(Boolean result) {
            this.result = result;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
