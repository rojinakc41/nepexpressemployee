package com.nepexpress.employeecourierapplication.todaysBasket;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveActivityResponse {

    @SerializedName("Result")
    @Expose
    private String result;

    @SerializedName("Message")
    @Expose
    private String message;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
