package com.nepexpress.employeecourierapplication.todaysBasket;

import java.util.List;

/**
 * Created by rojin on 1/18/2018.
 */

public class TodaysBasketPresenter implements TodaysBasketContract.onGetTodayBasketListener,TodaysBasketContract.Presenter {
    TodaysBasketInteractor basketInteractor;
    TodaysBasketContract.View view;

    public TodaysBasketPresenter(TodaysBasketContract.View view) {
        this.view=view;
        basketInteractor=new TodaysBasketInteractor(this);

    }

    @Override
    public void getTodayBasket(String username, String password,String url) {
        basketInteractor.getTodaysBasketFromDatabse(username,password, url);

    }

    @Override
    public void onGetBasketListSuccess(List<Courier> courierList) {
        view.onGetBasketListSuccess(courierList);

    }

    @Override
    public void onGetBasketListFailure(String message) {
        view.onGetBasketListFailure(message);

    }
}
