package com.nepexpress.employeecourierapplication.todaysBasket;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Activity {
    //old one
    @SerializedName("DId")
    @Expose
    private String dId;
    @SerializedName("OrderId")
    @Expose
    private String orderId;
    @SerializedName("Activity")
    @Expose
    private String activity;
    @SerializedName("Staff")
    @Expose
    private String staff;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("Timestamp")
    @Expose
    private String timestamp;

    public String getdId() {
        return dId;
    }

    public void setdId(String dId) {
        this.dId = dId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    //new one
/*

@SerializedName("DId")
@Expose
private Integer dId;
@SerializedName("OrderId")
@Expose
private String orderId;
@SerializedName("Name")
@Expose
private String name;
@SerializedName("Address")
@Expose
private String address;
@SerializedName("Amount")
@Expose
private Integer amount;
@SerializedName("Contact")
@Expose
private String contact;
@SerializedName("Quantity")
@Expose
private String quantity;
@SerializedName("Status")
@Expose
private String status;
@SerializedName("Staff")
@Expose
private String staff;
@SerializedName("Parcel")
@Expose
private String parcel;
@SerializedName("Date")
@Expose
private String date;
@SerializedName("Comment")
@Expose
private String comment;
@SerializedName("Sender")
@Expose
private String sender;
@SerializedName("SenderLatang")
@Expose
private String senderLatang;
@SerializedName("ReceiverLatang")
@Expose
private String receiverLatang;
@SerializedName("Weight")
@Expose
private Integer weight;

public Integer getDId() {
return dId;
}

public void setDId(Integer dId) {
this.dId = dId;
}

public String getOrderId() {
return orderId;
}

public void setOrderId(String orderId) {
this.orderId = orderId;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getAddress() {
return address;
}

public void setAddress(String address) {
this.address = address;
}

public Integer getAmount() {
return amount;
}

public void setAmount(Integer amount) {
this.amount = amount;
}

public String getContact() {
return contact;
}

public void setContact(String contact) {
this.contact = contact;
}

public String getQuantity() {
return quantity;
}

public void setQuantity(String quantity) {
this.quantity = quantity;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getStaff() {
return staff;
}

public void setStaff(String staff) {
this.staff = staff;
}

public String getParcel() {
return parcel;
}

public void setParcel(String parcel) {
this.parcel = parcel;
}

public String getDate() {
return date;
}

public void setDate(String date) {
this.date = date;
}

public String getComment() {
return comment;
}

public void setComment(String comment) {
this.comment = comment;
}

public String getSender() {
return sender;
}

public void setSender(String sender) {
this.sender = sender;
}

public String getSenderLatang() {
return senderLatang;
}

public void setSenderLatang(String senderLatang) {
this.senderLatang = senderLatang;
}

public String getReceiverLatang() {
return receiverLatang;
}

public void setReceiverLatang(String receiverLatang) {
this.receiverLatang = receiverLatang;
}

public Integer getWeight() {
return weight;
}

public void setWeight(Integer weight) {
this.weight = weight;
}
*/
}
