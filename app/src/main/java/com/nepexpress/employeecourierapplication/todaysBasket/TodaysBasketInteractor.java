package com.nepexpress.employeecourierapplication.todaysBasket;

import android.util.Log;

import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;

import java.util.List;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rojin on 1/18/2018.
 */

public class TodaysBasketInteractor implements TodaysBasketContract.Interactor {

    TodaysBasketContract.onGetTodayBasketListener basketListener;

    private final String TAG = TodaysBasketInteractor.class.getSimpleName();
    CourierService courierService;

    public TodaysBasketInteractor(TodaysBasketContract.onGetTodayBasketListener basketListener) {
        this.basketListener = basketListener;
        courierService = RetrofitApiClient.getCourierService();

    }

    @Override
    public void getTodaysBasketFromDatabse(String userName, String password, String url) {

        String credentials = Credentials.basic(userName, password);
        Log.d(TAG, "the credential is:" + credentials);
        Log.d(TAG, "the usernmae is:" + userName);
        Log.d(TAG, "the password is:" + password);
        Call<List<Courier>> call = courierService.downloadCourierList(credentials, url);

        call.enqueue(new Callback<List<Courier>>() {
            @Override
            public void onResponse(Call<List<Courier>> call, Response<List<Courier>> response) {
                if (response.isSuccessful()) {
                    List<Courier> courierList = response.body();
                    Log.e("response size", String.valueOf(courierList.size()));
                    Log.d(TAG, "response succesful::" + response);
                    Log.d(TAG, "courier siz eis::" + courierList.size());
                    basketListener.onGetBasketListSuccess(courierList);


                } else {
                    Log.d(TAG, "error getting response");
                    basketListener.onGetBasketListFailure("Error getting response");
                }
            }

            @Override
            public void onFailure(Call<List<Courier>> call, Throwable t) {
                Log.d(TAG, "response failed::" + t.getMessage());
                basketListener.onGetBasketListFailure("error::" + t.getMessage());

            }
        });
    }

}
