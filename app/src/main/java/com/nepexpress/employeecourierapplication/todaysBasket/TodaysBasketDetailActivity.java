package com.nepexpress.employeecourierapplication.todaysBasket;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;
import com.nepexpress.employeecourierapplication.base.BaseActivity;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.showMap.MapsActivity;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodaysBasketDetailActivity extends BaseActivity {

    Courier courier;
    public static final String TAG = TodaysBasketDetailActivity.class.getSimpleName();
    public static final String KEY_PARCEL_DETAIL = "parcelDetail";

    TextView orderIdTv, orderNameTv, addressTv, phoneTv, amountTv, quantity, date, parcel;
    EditText commentEt;
    Button saveCourier, navigateToMap; ///addImage;
    Spinner statusCourier;
    CourierDatabse databse;
    String comment, courierState;
    Boolean courierSaveStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todays_basket_detail);
        setUpToolbar("Details");
        courier = (Courier) getIntent().getSerializableExtra(KEY_PARCEL_DETAIL);
        initView();
        defineView();
        bindView();
        addClickListener();

    }

    private void initView() {

        databse = CourierDatabse.getAppDatabase(getApplicationContext());
    }

    private void defineView() {
        orderIdTv = findViewById(R.id.order_id_tv);
        parcel = findViewById(R.id.order_parcel);
        orderNameTv = findViewById(R.id.order_name_tv);
        addressTv = findViewById(R.id.order_address_tv);
        phoneTv = findViewById(R.id.show_phone_no);
        amountTv = findViewById(R.id.order_amount);
        quantity = findViewById(R.id.order_quantity);
        saveCourier = findViewById(R.id.save_courier);
        commentEt = findViewById(R.id.show_comment_et);
        date = findViewById(R.id.show_date);
        navigateToMap = findViewById(R.id.navigate_to_latlng);
        //   addImage = findViewById(R.id.add_image);
        statusCourier = findViewById(R.id.show_status_spinner);

    }

    private void bindView() {
        orderIdTv.setText("OrderId: " + courier.getOrderId());
        orderNameTv.setText("Name: " + courier.getName());
        addressTv.setText("Address: " + courier.getAddress());
        phoneTv.setText("Contact: " + courier.getContact());
        date.setText("Date: " + courier.getDate());
        amountTv.setText("Amount: " + courier.getAmount());
        quantity.setText("Quantity: " + courier.getQuantity());

        parcel.setText("Parcel: " + courier.getParcel());

        commentEt.setText(courier.getComment());

    }

    private void addClickListener() {

       /* addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
        navigateToMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
                boolean enabled = service
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
                if (!enabled) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 1);
                } else {
                    Intent intent = new Intent(TodaysBasketDetailActivity.this, MapsActivity.class);
                    if (!TextUtils.isEmpty(courier.getReceiverLatLng()))
                        intent.putExtra(MapsActivity.ARG_RECEIVER_LATLNG, courier.getReceiverLatLng());
                    if (!TextUtils.isEmpty(courier.getSenderLatLng()))
                        intent.putExtra(MapsActivity.ARG_SENDER_LATLNG, courier.getSenderLatLng());
                    startActivity(intent);
                }
            }
        });
        saveCourier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comment = commentEt.getText().toString();
                if (TextUtils.isEmpty(courierState))
                    Toast.makeText(TodaysBasketDetailActivity.this, "Please select status of courier", Toast.LENGTH_SHORT).show();

                else {
                    Long curentTime = System.currentTimeMillis();
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy ");
                    Date resultdate = new Date(curentTime);
                    String date = sdf.format(resultdate);

//                    Log.e("orderId", courier.getOrderId());
                    databse.courierDAO().updateCourier(true,
                            courier.getDId(), courierState,
                            comment,
                            date);
                    onBackPressed();



                    //  Toast.makeText(TodaysBasketDetailActivity.this, "sucessfully updated", Toast.LENGTH_SHORT).show();


                }
            }
        });

        statusCourier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i > 0) {
                    courierState = statusCourier.getSelectedItem().toString();
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_todays_basket_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_make_phone_call) {
            dialNumber();
            return true;
        } else if (id == R.id.action_send_message) {
            sendMessage();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendMessage() {

        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", courier.getContact(), null)));
    }

    private void makeAPhoneCall() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + courier.getContact()));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);

    }

    public void dialNumber() {
        String phoneNumber = String.format("tel: %s",
                courier.getContact());
        // Create the intent.
        Intent dialIntent = new Intent(Intent.ACTION_DIAL);
        // Set the data for the intent as the phone number.
        dialIntent.setData(Uri.parse(phoneNumber));
        // If package resolves to an app, send intent.
        if (dialIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(dialIntent);
        } else {
            Log.e(TAG, "Can't resolve app for ACTION_DIAL Intent.");
        }
    }


    CourierService courierService;
    SharedPrefUtil sharedPrefUtil;

    private void sendActivityToServer(final Activity activity) {
        showDialog("Sending activity to server");
        sharedPrefUtil = new SharedPrefUtil(this);
        courierService = RetrofitApiClient.getCourierService();
        String credentials = Credentials.basic(sharedPrefUtil.getStrings(AppText.KEY_USER_NAME),
                sharedPrefUtil.getStrings(AppText.KEY_USER_PASSWORD));
        Call<SaveActivityResponse> activityResponseCall = courierService.saveActivity(credentials, activity);
        activityResponseCall.enqueue(new Callback<SaveActivityResponse>() {
            @Override
            public void onResponse(Call<SaveActivityResponse> call, Response<SaveActivityResponse> response) {
                if (response.isSuccessful()) {

                    hideDialog();


                    Toast.makeText(TodaysBasketDetailActivity.this,
                            response.body().getMessage(),
                            Toast.LENGTH_SHORT).show();
                } else {
                    hideDialog();
                    Toast.makeText(TodaysBasketDetailActivity.this, "Error sending to server!! Try again", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<SaveActivityResponse> call, Throwable t) {
                hideDialog();
                Toast.makeText(TodaysBasketDetailActivity.this, "Error sending to server!! Try again", Toast.LENGTH_SHORT).show();


            }
        });
    }
}
