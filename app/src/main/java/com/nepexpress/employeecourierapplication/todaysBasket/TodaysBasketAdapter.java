package com.nepexpress.employeecourierapplication.todaysBasket;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nepexpress.employeecourierapplication.R;

import java.util.List;

/**
 * Created by rojin on 1/16/2018.
 */

public class TodaysBasketAdapter extends RecyclerView.Adapter<TodaysBasketAdapter.ViewHolder> {

    List<Courier> basketList;
    onParcelClickListener parcelClickListener;

    public TodaysBasketAdapter(List<Courier> basketList, onParcelClickListener parcelClickListener) {
        this.basketList = basketList;
        this.parcelClickListener=parcelClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_todays_basket,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView(basketList.get(position));
    }


    @Override
    public int getItemCount() {
        return basketList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView orderId,orderName,orderAddress,orderComment;
        public ViewHolder(View itemView) {
            super(itemView);
            orderId=itemView.findViewById(R.id.order_id_tv);
            orderName=itemView.findViewById(R.id.order_name_tv);
            orderAddress=itemView.findViewById(R.id.order_address_tv);
            orderComment=itemView.findViewById(R.id.order_comment_tv);
        }
        public void bindView(final Courier basket){

            orderId.setText(basket.getOrderId()+"/"+basket.getParcel());
            orderName.setText(basket.getName());
            orderAddress.setText(basket.getAddress());
            orderComment.setText(basket.getComment());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    parcelClickListener.addParcelClickListener(basket);
                }
            });
        }
    }
    public interface onParcelClickListener{
        void addParcelClickListener(Courier courier);
    }
}
