package com.nepexpress.employeecourierapplication.todaysBasket;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TodaysBasketFragment.OnTodayBasketFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TodaysBasketFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TodaysBasketFragment extends Fragment {
    RecyclerView showBasketRv;
    CourierDatabse databse;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM_URL_TO_CONNECT = "urlToConnect";

    // TODO: Rename and change types of parameters
    private String urlToConnect;


    private OnTodayBasketFragmentInteractionListener mListener;
    List<Courier> basketList=new ArrayList<>();

    public TodaysBasketFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param urlToConnect Parameter 1.

     * @return A new instance of fragment TodaysBasketFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TodaysBasketFragment newInstance(String urlToConnect) {
        TodaysBasketFragment fragment = new TodaysBasketFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_URL_TO_CONNECT, urlToConnect);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            urlToConnect = getArguments().getString(ARG_PARAM_URL_TO_CONNECT);
        }
        initView();
    }

    private void initView(){
        databse=CourierDatabse.getAppDatabase(getContext());

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_todays_basket, container, false);
        defineView(view);
        setUpCourier();
        return view;
    }

    private void defineView(View view){
        showBasketRv=view.findViewById(R.id.show_todays_basket);
    }

    private void setUpCourier(){
        LinearLayoutManager manager=new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        showBasketRv.setLayoutManager(manager);
        showBasketRv.setAdapter(new TodaysBasketAdapter(setUpBasketList(), new TodaysBasketAdapter.onParcelClickListener() {
            @Override
            public void addParcelClickListener(Courier courier) {
               onButtonPressed(courier);
            }
        }));
    }

    private List<Courier> setUpBasketList(){

        basketList=databse.courierDAO().getCourierYetToBeParceled(false);
        // for(int i=0;i<basketList.size();i++)
        // courierAddress.add(basketList.get(i).getAddress());
        //   Toast.makeText(this, "the size of your basket is:::"+basketList.size(), Toast.LENGTH_SHORT).show();
        return basketList;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Courier courier) {
        if (mListener != null) {
            mListener.onCourierClicked(courier);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTodayBasketFragmentInteractionListener) {
            mListener = (OnTodayBasketFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnTodayBasketFragmentInteractionListener {
        // TODO: Update argument type and name
        void onCourierClicked(Courier courier);
    }
}
