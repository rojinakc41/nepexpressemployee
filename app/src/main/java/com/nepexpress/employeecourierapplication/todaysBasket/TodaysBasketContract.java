package com.nepexpress.employeecourierapplication.todaysBasket;

import java.util.List;

/**
 * Created by rojin on 1/18/2018.
 */

public interface TodaysBasketContract {

    public interface View{
        void onGetBasketListSuccess(List<Courier> courierList);
        void onGetBasketListFailure(String message);
    }
    interface Presenter{
        void getTodayBasket(String username,String password,String url);
    }
    interface Interactor{
        void getTodaysBasketFromDatabse(String userName,String password,String url);
    }
    interface onGetTodayBasketListener{
        void onGetBasketListSuccess(List<Courier> courierList);
        void onGetBasketListFailure(String message);
    }
}
