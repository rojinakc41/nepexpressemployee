package com.nepexpress.employeecourierapplication.todaysBasket;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by rojin on 1/16/2018.
 */


@Entity(tableName = "courier")
public class Courier implements Serializable {

    @NonNull
    @PrimaryKey
    @SerializedName("DId")
    @Expose
    private String DId;

    public String getDId() {
        return DId;
    }

    public void setDId(String DId) {
        this.DId = DId;
    }

    @ColumnInfo
    @SerializedName("OrderId")
    @Expose
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @ColumnInfo
    @SerializedName("Name")
    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ColumnInfo
    @SerializedName("Address")
    @Expose
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @ColumnInfo
    @SerializedName("Amount")
    @Expose
    private String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @ColumnInfo
    @SerializedName("Contact")
    @Expose
    private String contact;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @ColumnInfo
    @SerializedName("Quantity")
    @Expose
    private String quantity;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @ColumnInfo
    @SerializedName("Status")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @ColumnInfo
    @SerializedName("Staff")
    @Expose
    private String staff;

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    @ColumnInfo
    @SerializedName("Parcel")
    @Expose
    private String parcel;

    public String getParcel() {
        return parcel;
    }

    public void setParcel(String parcel) {
        this.parcel = parcel;
    }

    @ColumnInfo
    @SerializedName("Date")
    @Expose
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    @ColumnInfo
    @SerializedName("Comment")
    @Expose
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    @ColumnInfo
    @SerializedName("Sender")
    @Expose
    private String sender;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @SerializedName("SenderLatang")
    @Expose
    @ColumnInfo
    private String senderLatLng;

    public String getSenderLatLng() {
        return senderLatLng;
    }

    public void setSenderLatLng(String senderLatLng) {
        this.senderLatLng = senderLatLng;
    }


    @SerializedName("ReceiverLatang")
    @Expose
    @ColumnInfo
    private String receiverLatLng;

    public String getReceiverLatLng() {
        return receiverLatLng;
    }

    public void setReceiverLatLng(String receiverLatLng) {
        this.receiverLatLng = receiverLatLng;
    }

    @ColumnInfo
    private Boolean isSavedStatus = false;

    public Boolean getSavedStatus() {
        return isSavedStatus;
    }

    public void setSavedStatus(Boolean savedStatus) {
        isSavedStatus = savedStatus;
    }

    //added by jeevan

    @Ignore
    @SerializedName("Distance")
    @Expose
    private Double distance;


    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
