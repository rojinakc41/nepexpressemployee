package com.nepexpress.employeecourierapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgentParameter {

    @SerializedName("CountryCode")
    @Expose
    private String CountryCode;

    @SerializedName("TotalWeight")
    @Expose
    private String TotalWeight;


    public AgentParameter(String countryCode, String totalWeight) {
        CountryCode = countryCode;
        TotalWeight = totalWeight;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getTotalWeight() {
        return TotalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        TotalWeight = totalWeight;
    }
}
