package com.nepexpress.employeecourierapplication;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;
import com.nepexpress.employeecourierapplication.base.BaseActivity;
import com.nepexpress.employeecourierapplication.createAgent.CreateAgentActivity;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.history.ParcelHistoryActivity;
import com.nepexpress.employeecourierapplication.login.LoginActivity;
import com.nepexpress.employeecourierapplication.mainMap.MainDashBoardFragment;
import com.nepexpress.employeecourierapplication.oneSignalNotification.NotiDeviceModel;
import com.nepexpress.employeecourierapplication.oneSignalNotification.NotificationActivity;
import com.nepexpress.employeecourierapplication.oneSignalNotification.OneSignalNotification;
import com.nepexpress.employeecourierapplication.todaysBasket.Courier;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketContract;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketDetailActivity;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketFragment;
import com.nepexpress.employeecourierapplication.todaysBasket.TodaysBasketPresenter;
import com.nepexpress.employeecourierapplication.trackEmployee.AlarmReceiver;
import com.nepexpress.employeecourierapplication.trackParcel.TrackParcelActivity;
import com.nepexpress.employeecourierapplication.uploadBasket.UploadBasketContract;
import com.nepexpress.employeecourierapplication.uploadBasket.UploadBasketPresenter;
import com.nepexpress.employeecourierapplication.uploadBasket.UploadFragment;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeMainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, TodaysBasketContract.View
        , TodaysBasketFragment.OnTodayBasketFragmentInteractionListener,
        UploadBasketContract.View,
        UploadFragment.OnUploadFragmentInteractionListener

{


    DrawerLayout drawer;
    NavigationView navigationView;
  //  GetPackagesDatabase getPackagesDatabase;

    String userNameStr, passwordStr, loginTypeStr;
    FrameLayout mainContainer;
    TextView showNameInSideNav, showUserNameTv, showTotalCourierTv, showDeliveredCourierTv, showCanceledCourierTv, showPendingCourierTv, showMessage;
    Toolbar toolbar;
    CourierDatabse databse;
    Button sortAlphabetically;


    SharedPrefUtil prefUtil;
    Button downloadBtn;


    UploadBasketPresenter uploadBasketPresenter;
    TodaysBasketPresenter todaysBasketPresenter;

    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_main);

        initializeLocation();
        initView();
        defineView();
        bindView();
    }

    private void initView() {
        prefUtil = new SharedPrefUtil(this);
        databse = CourierDatabse.getAppDatabase(getApplicationContext());
        //getPackagesDatabase = GetPackagesDatabase.getDatabase(getApplicationContext());
        // courierAddress=new ArrayList<>();
        todaysBasketPresenter = new TodaysBasketPresenter(this);
        uploadBasketPresenter = new UploadBasketPresenter(this,this);

        userNameStr = prefUtil.getStrings(AppText.KEY_USER_NAME);
        passwordStr = prefUtil.getStrings(AppText.KEY_USER_PASSWORD);
        loginTypeStr = prefUtil.getStrings(AppText.KEY_LOGIN_TYPE);
        calculateDateForSession();

    }

    private void defineView() {
        showUserNameTv = findViewById(R.id.show_user_name);

        downloadBtn = findViewById(R.id.download_btn);

        showTotalCourierTv = findViewById(R.id.show_total_courier);
        showDeliveredCourierTv = findViewById(R.id.show_delivered_courier);
        showCanceledCourierTv = findViewById(R.id.show_rejected_courier);
        showPendingCourierTv = findViewById(R.id.show_pending_courier);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        mainContainer = findViewById(R.id.main_container);

        showMessage = findViewById(R.id.show_message);


        sortAlphabetically = findViewById(R.id.sort_alphabetically_btn);


    }

    private void bindView() {
        setSupportActionBar(toolbar);
        //  getSupportActionBar().setTitle(prefUtil.getStrings(AppText.KEY_USER_NAME));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        showNameInSideNav = (TextView) header.findViewById(R.id.show_username_nav);
        showNameInSideNav.setText(userNameStr);

        showUserNameTv.setText(userNameStr);

        ////replace the main fragment
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent, 1);
        } else {
            downloadBtn.setVisibility(View.GONE);
            showMessage.setVisibility(View.GONE);

            /*if (prefUtil.getPickMode() != null && prefUtil.getPickMode() == true) {

                Intent intent = new Intent(EmployeeMainActivity.this, GetPackageDetailActivity.class);
                startActivity(intent);

            } else {
*/
            MainDashBoardFragment fragment = new MainDashBoardFragment();
            FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
            fragmentManager.replace(R.id.main_container, fragment).commit();
  /*          }*/
        }
        setUpCouriers();
        postNotificationDeviceId();
//        if (databse.courierDAO().getAllCourier().size() > 0) {
//            downloadBtn.setVisibility(View.GONE);
//            showMessage.setVisibility(View.GONE);
//            LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
//            boolean enabled = service
//                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
//            if (!enabled) {
//                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                startActivityForResult(intent, 1);
//            } else {
//                MainDashBoardFragment fragment = new MainDashBoardFragment();
//                // TodaysBasketFragment fragment=new TodaysBasketFragment();
//                FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
//                fragmentManager.replace(R.id.main_container, fragment).commit();
//            }
//            setUpCouriers();
//        } else {
//            downloadBtn.setVisibility(View.VISIBLE);
//            showMessage.setVisibility(View.VISIBLE);
//
//            showMessage.setText("Seem like you donot have any couriers!! Download it!!!");
//            downloadBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    showDialog("Loading your courier");
//                    todaysBasketPresenter.getTodayBasket(userNameStr, passwordStr, AppText.URL_DOWNLOAD_BASKET);
//                }
//            });
//        }

        //tracking the live location
        // new PostEmployeeLiveLocation(this).execute((Void[]) null);
    }

    private void calculateDateForSession() {
        String sessionDate = prefUtil.getSessionDate();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String todayDate = format.format(new Date());
        if (sessionDate == null) {
            prefUtil.insertTodayDate(todayDate);
        }
        sessionDate = prefUtil.getSessionDate();
        try {
            Date dateSession = format.parse(sessionDate);
            Date dateNew = format.parse(todayDate);
            long difference = dateNew.getTime() - dateSession.getTime();
            float daysBetween = (difference / (1000 * 60 * 60 * 24));
               /* You can also convert the milliseconds to days using this method
                * float daysBetween =
                *         TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS)
                */
            if (daysBetween > 0) {
                prefUtil.insertTodayDate(todayDate);
//                getPackagesDatabase.getPackageDao().deleteAllPackages();
                databse.getPackageDao().deleteAllPackages();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpCouriers() {
        //   databse.courierDAO().deleteAllCourier();
        int delivered = databse.courierDAO().getNumberOfDeliveredCourier();
        int cancelled = databse.courierDAO().getNumberOfCanceledAndRejectedCourier();
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int totalDelivered = databse.getPackageDao().getNumberOfDeliveredPackage()+delivered;
        showDeliveredCourierTv.setText(String.valueOf(totalDelivered));

        int totalCancelled = databse.getPackageDao().getNumberOfCanceledAndRejectedPackage() + cancelled;
        showCanceledCourierTv.setText(String.valueOf(totalCancelled));

        //  Toast.makeText(this, "username is:"+userNameStr, Toast.LENGTH_SHORT).show();
        /*showTotalCourierTv.setText(String.valueOf(databse.courierDAO().getAllCourier().size()));*/
        //showCanceledCourierTv.setText(String.valueOf(databse.courierDAO().getNumberOfCanceledAndRejectedCourier()));
        //showDeliveredCourierTv.setText(String.valueOf(databse.courierDAO().getNumberOfDeliveredCourier()));
        /*showPendingCourierTv.setText(String.valueOf(databse.courierDAO().getNumberOfPendingCourier()));*/

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            downloadBtn.setVisibility(View.GONE);
            showMessage.setVisibility(View.GONE);
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            downloadBtn.setVisibility(View.GONE);
            showMessage.setVisibility(View.GONE);
            LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean enabled = service
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!enabled) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 1);
            } else {
                MainDashBoardFragment fragment = new MainDashBoardFragment();
                // TodaysBasketFragment fragment=new TodaysBasketFragment();
                FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
                fragmentManager.addToBackStack(null);
                fragmentManager.replace(R.id.main_container, fragment).commit();
            }
        } else if (id == R.id.nav_your_basket) {

            if (databse.courierDAO().getAllCourier().size() > 0) {
                downloadBtn.setVisibility(View.GONE);
                showMessage.setVisibility(View.GONE);
                LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
                boolean enabled = service
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
                if (!enabled) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 1);
                } else {
                    //MainDashBoardFragment fragment = new MainDashBoardFragment();
                    TodaysBasketFragment fragment = new TodaysBasketFragment();
                    FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
                    fragmentManager.replace(R.id.main_container, fragment).commit();
                }
                setUpCouriers();
            } else {
                downloadBtn.setVisibility(View.VISIBLE);
                showMessage.setVisibility(View.VISIBLE);
                todayBasketFragment();
//                TodaysBasketFragment fragment = new TodaysBasketFragment();
//                FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
//                fragmentManager.addToBackStack(null);
//                fragmentManager.replace(R.id.main_container, fragment).commit();

                showMessage.setText("Seem like you donot have any couriers!! Download it!!!");
                downloadBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showDialog("Loading your courier");
                        todaysBasketPresenter.getTodayBasket(userNameStr, passwordStr, AppText.URL_DOWNLOAD_BASKET);
                    }
                });
            }
        } else if (id == R.id.nav_track_parcel) {
            // Handle the camera action
            Intent intent = new Intent(this, TrackParcelActivity.class);
            intent.putExtra(TrackParcelActivity.TRACK_PARCEL_URL, AppText.URL_TRACK_PARCEL_EMPLOYEE);
            startActivity(intent);
        } else if (id == R.id.nav_parcel_history) {
            startActivity(new Intent(EmployeeMainActivity.this, ParcelHistoryActivity.class));

        } else if (id == R.id.nav_upload) {

            if (databse.courierDAO().getCourierListOrderFulfilled().size() > 0) {
                showMessage.setVisibility(View.GONE);
                UploadFragment uploadFragment = new UploadFragment().newInstance(userNameStr, passwordStr);
                FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
                fragmentManager.replace(R.id.main_container, uploadFragment).commit();
            } else {

                Toast.makeText(this, "Seems like you haven't done anything to upload courier", Toast.LENGTH_SHORT).show();

            }


        } else if (id == R.id.nav_download) {
            showDialog("Loading your courier");
            todaysBasketPresenter.getTodayBasket(userNameStr, passwordStr, AppText.URL_DOWNLOAD_BASKET);

        } else if (id == R.id.nav_user) {

            LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean enabled = service
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!enabled) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 2);
            } else {
                Intent intent = new Intent(this, CreateAgentActivity.class);
                startActivity(intent);
            }

        } else if (id == R.id.nav_logout) {
            prefUtil.saveStrings(AppText.KEY_USER_NAME, "");
            prefUtil.saveStrings(AppText.KEY_USER_PASSWORD, "");
            prefUtil.saveStrings(AppText.KEY_LOGIN_TYPE, "");
            prefUtil.insertNotificationStatus(false);
            prefUtil.insertTodayDate(null);
            prefUtil.insertPackageThreadStatus(false);
            prefUtil.insertTrackThreadStatus(false);
            databse.courierDAO().deleteAllCourier();
            databse.getPackageDao().deleteAllPackages();

            startActivity(new Intent(EmployeeMainActivity.this, LoginActivity.class));
            finish();
            Toast.makeText(this, "Successfully Logged Out", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_notification) {

            Intent intent = new Intent(EmployeeMainActivity.this, NotificationActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
        // Toast.makeText(this, "refreshed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetBasketListSuccess(List<Courier> courierList) {
        databse.courierDAO().deleteAllCourier();
        hideDialog();
        databse.courierDAO().insertAllCourier(courierList);
        downloadBtn.setVisibility(View.GONE);
        showMessage.setVisibility(View.GONE);

        todayBasketFragment();

    }

    private void todayBasketFragment() {

        TodaysBasketFragment fragment = new TodaysBasketFragment();
        FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
        fragmentManager.addToBackStack(null);
        fragmentManager.replace(R.id.main_container, fragment).commit();

    }

    @Override
    public void onGetBasketListFailure(String message) {
        hideDialog();
        Toast.makeText(this, "Couldn't get any response from tne server", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onCourierClicked(Courier courier) {
        Intent intent = new Intent(EmployeeMainActivity.this, TodaysBasketDetailActivity.class);
        intent.putExtra(TodaysBasketDetailActivity.KEY_PARCEL_DETAIL, courier);
        startActivity(intent);
    }

    @Override
    public void onUploadBasketSuccess(List<Courier> courierList) {
        hideDialog();

        Toast.makeText(this, "success:", Toast.LENGTH_SHORT).show();

        for (Courier courier : courierList) {
            databse.courierDAO().deleteIndividualCourier(courier);
        }

        finish();
        startActivity(getIntent());


    }

    @Override
    public void onUploadBasketFailure(String message) {

        hideDialog();
        Toast.makeText(this, "failed:" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUploadClicked(String credential, List<Courier> courierListToUpload) {
        showDialog("Uploading your couriers.....");
        uploadBasketPresenter.uploadBasket(credential, AppText.URL_UPDATE_BASKET, courierListToUpload);
    }

    private void initializeLocation() {
        if (alarmManager == null) {
            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(this, AlarmReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 30000,
                    pendingIntent);
        }
    }

    private void postNotificationDeviceId() {
        if (prefUtil.getNotificationStatus() != null && prefUtil.getNotificationStatus() == true) {
            return;
        }
        CourierService service = RetrofitApiClient.getCourierService();
        final String deviceId = initializeOneSignal();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String dateTime = sdf.format(new Date()).split(" ")[0] + "T" + sdf.format(new Date()).split(" ")[1];
        final String crediantals = Credentials.basic(userNameStr, passwordStr);
        service.postNotificationDeviceId(crediantals, prefUtil.getStrings(AppText.KEY_USER_NAME), null,
                loginTypeStr, deviceId, null, dateTime).enqueue(new Callback<NotiDeviceModel>() {
            @Override
            public void onResponse(Call<NotiDeviceModel> call, Response<NotiDeviceModel> response) {
                if (response.isSuccessful()) {
                    prefUtil.insertNotificationStatus(true);
                }
            }

            @Override
            public void onFailure(Call<NotiDeviceModel> call, Throwable t) {

            }
        });
    }

    private String initializeOneSignal() {
        OneSignalNotification.InitilizeOneSignal(this.getApplication());
        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        return status.getSubscriptionStatus().getUserId();
    }


}
