package com.nepexpress.employeecourierapplication.trackParcel;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nepexpress.employeecourierapplication.R;

import java.util.List;

public class TrackParcelAdapter extends RecyclerView.Adapter<TrackParcelAdapter.ViewHolder>{
    List<TrackParcel> trackParcelList;

    public TrackParcelAdapter(List<TrackParcel> trackParcelList) {
        this.trackParcelList = trackParcelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_track_parcel,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bindView(trackParcelList.get(position));
    }

    @Override
    public int getItemCount() {
        return trackParcelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView showActivity,showComment,showOrderId,showStaffAssigned,showTimeStamp,showParcelInfoTv;
       
        public ViewHolder(View itemView) {
            super(itemView);

            showActivity=itemView.findViewById(R.id.show_activity);
            showComment=itemView.findViewById(R.id.show_comment);
            showOrderId=itemView.findViewById(R.id.show_order_id);
            showStaffAssigned=itemView.findViewById(R.id.show_staff_assigned);
            showTimeStamp=itemView.findViewById(R.id.show_time_stamp);
        }

        public void bindView(TrackParcel trackParcel){
            showActivity.setText(trackParcel.getActivity1());
            showComment.setText(trackParcel.getComment());
            showOrderId.setText(trackParcel.getOrderId());
            showStaffAssigned.setText(trackParcel.getStaff());
            showTimeStamp.setText(trackParcel.getTimestamp());
        }
    }
}
