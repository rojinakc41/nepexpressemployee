package com.nepexpress.employeecourierapplication.trackParcel;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.api.CourierService;
import com.nepexpress.employeecourierapplication.api.RetrofitApiClient;
import com.nepexpress.employeecourierapplication.util.AppText;
import com.nepexpress.employeecourierapplication.util.SharedPrefUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackParcelActivity extends AppCompatActivity {

    EditText orderIdEt;
    Button trackParcelBtn;
    private String url,orderId;
    CourierService courierService;
    SharedPrefUtil prefUtil;
    String username,password;
    public static final String TRACK_PARCEL_URL="parcelUrl";
    ProgressDialog mProgressDialog;

    RecyclerView showTrackParcelList;

TextView showParcelInfoTv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_parcel);
        initView();
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            if(bundle.containsKey(TRACK_PARCEL_URL))
                url=bundle.getString(TRACK_PARCEL_URL);
        }
        defineView();addClickListener();
    }

    private void defineView(){
        orderIdEt=findViewById(R.id.order_id_et);
        trackParcelBtn=findViewById(R.id.track_parcel_btn);


        showParcelInfoTv=findViewById(R.id.show_parcel_information_tv);
        showTrackParcelList=findViewById(R.id.track_parcel_list);

        getSupportActionBar().setTitle("Track Parcel");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        showTrackParcelList.setLayoutManager(layoutManager);
    }
    private void initView(){
        courierService= RetrofitApiClient.getCourierService();
        prefUtil=new SharedPrefUtil(this);
        username=prefUtil.getStrings(AppText.KEY_USER_NAME);
        password=prefUtil.getStrings(AppText.KEY_USER_PASSWORD);

    }
    private void addClickListener(){
        trackParcelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderId=orderIdEt.getText().toString();
                if(TextUtils.isEmpty(orderId))
                    orderIdEt.setError("Required");
                else
                    trackParcel();

            }
        });
    }


    private void trackParcel(){
        showDialog("Tracking your parcel....");
        String cre= okhttp3.Credentials.basic(username,password);
        Call<List<TrackParcel>> call=courierService.trackParcel(cre,url,orderId);
        call.enqueue(new Callback<List<TrackParcel>>() {
            @Override
            public void onResponse(Call<List<TrackParcel>> call, Response<List<TrackParcel>> response) {
                hideDialog();
                if(response.isSuccessful()){

                    if(response.body().size()>0){
                        TrackParcel trackParcel=response.body().get(0);
                        showParcelInfoTv.setVisibility(View.VISIBLE);
                        showParcelInfoTv.setText("The parcel information:");
                        showTrackParcelList.setVisibility(View.VISIBLE);
                        TrackParcelAdapter trackParcelAdapter=new TrackParcelAdapter(response.body());
                        showTrackParcelList.setAdapter(trackParcelAdapter);





                    }else {
                       showTrackParcelList.setVisibility(View.GONE);
                        showParcelInfoTv.setVisibility(View.VISIBLE);
                        showParcelInfoTv.setText("No parcel found of the given ID..Try entering new ID");
                    }

                }else{

                    Toast.makeText(TrackParcelActivity.this, "Error in esponse", Toast.LENGTH_SHORT).show();
                   showTrackParcelList.setVisibility(View.GONE);
                    showParcelInfoTv.setVisibility(View.VISIBLE);
                    showParcelInfoTv.setText("Couldn't get the information");
                }
            }

            @Override
            public void onFailure(Call<List<TrackParcel>> call, Throwable t) {
                Toast.makeText(TrackParcelActivity.this, "erroe", Toast.LENGTH_SHORT).show();
                hideDialog();

                showTrackParcelList.setVisibility(View.GONE);
                showParcelInfoTv.setVisibility(View.VISIBLE);
                showParcelInfoTv.setText("Error while getting data from server");
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


    protected void showDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    protected void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }


    }

}
