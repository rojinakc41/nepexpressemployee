package com.nepexpress.employeecourierapplication.trackParcel;

import com.google.gson.annotations.SerializedName;

public class TrackParcel {

    @SerializedName("ActivityId")
      private String ActivityId;

    @SerializedName("DId")
      private String DId;

    @SerializedName("OrderId")
      private String OrderId;

    @SerializedName("Activity1")
      private String Activity1;

    @SerializedName("Comment")
      private String Comment;

    @SerializedName("Staff")
      private String Staff;

    @SerializedName("Timestamp")
      private String Timestamp;

    @SerializedName("clientId")
      private String clientId;

    @SerializedName("requestId")
      private String requestId;

    public String getActivityId() {
        return ActivityId;
    }

    public void setActivityId(String activityId) {
        ActivityId = activityId;
    }

    public String getDId() {
        return DId;
    }

    public void setDId(String DId) {
        this.DId = DId;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getActivity1() {
        return Activity1;
    }

    public void setActivity1(String activity1) {
        Activity1 = activity1;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getStaff() {
        return Staff;
    }

    public void setStaff(String staff) {
        Staff = staff;
    }

    public String getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(String timestamp) {
        Timestamp = timestamp;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
