package com.nepexpress.employeecourierapplication.history;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.todaysBasket.Courier;

import java.util.List;

/**
 * Created by rojin on 1/16/2018.
 */

public class ParcelHistoryAdapter extends RecyclerView.Adapter<ParcelHistoryAdapter.ViewHolder> {

    List<Courier> courierList;


    public ParcelHistoryAdapter(List<Courier> courierList) {
        this.courierList = courierList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_todays_history,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bindView(courierList.get(position));
    }

    @Override
    public int getItemCount() {
        return courierList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView orderId,senderName,receiverName,status,orderAddress,orderComment;
        public ViewHolder(View itemView) {
            super(itemView);
            orderId=itemView.findViewById(R.id.order_id_tv);
            orderAddress=itemView.findViewById(R.id.order_adddress_tv);
            senderName=itemView.findViewById(R.id.sender_name_tv);
            receiverName=itemView.findViewById(R.id.receiver_id_tv);
            status=itemView.findViewById(R.id.order_status);

            orderComment=itemView.findViewById(R.id.order_comment_tv);
        }
        public void bindView(final Courier basket){
            orderId.setText("Courier Id:"+basket.getOrderId());
            orderAddress.setText("Address:"+basket.getAddress());
            senderName.setText("Sender Name:"+basket.getName());
            receiverName.setText("Receiver Name:"+basket.getDate());
            orderComment.setText("Comment:"+basket.getComment());

            if(basket.getStatus().equalsIgnoreCase("Held") || basket.getStatus().equalsIgnoreCase("Pending"))
                status.setTextColor(Color.parseColor("#FFFF00"));
            else if(basket.getStatus().equalsIgnoreCase("Delivered"))
                status.setTextColor(Color.parseColor("#008000"));
            else if(basket.getStatus().equalsIgnoreCase("Canceled")||basket.getStatus().equalsIgnoreCase("Rejected"))
                status.setTextColor(Color.parseColor("#ff0000"));


            status.setText("Status:"+basket.getStatus());

        }
    }


}

