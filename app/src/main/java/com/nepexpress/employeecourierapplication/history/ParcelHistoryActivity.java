package com.nepexpress.employeecourierapplication.history;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nepexpress.employeecourierapplication.R;
import com.nepexpress.employeecourierapplication.base.BaseActivity;
import com.nepexpress.employeecourierapplication.database.CourierDatabse;
import com.nepexpress.employeecourierapplication.todaysBasket.Courier;

import java.util.ArrayList;
import java.util.List;

public class ParcelHistoryActivity extends BaseActivity  {

    TextView showMessage,toDateTv;
    RecyclerView parcelHistory;
    CourierDatabse databse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_history);
        setUpToolbar("History");
        initView();
        defineView();
        setClickListener();
        bindView();

    }
    private void initView(){
        databse = CourierDatabse.getAppDatabase(getApplicationContext());
    }
    private void defineView(){
       // fromDateTv=findViewById(R.id.from_date_tv);
        showMessage=findViewById(R.id.show_message);
        parcelHistory=findViewById(R.id.show_history_rv);
    }
    private void bindView(){
        LinearLayoutManager manager=new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);


        if(setUpParcelList().size()>0){
            parcelHistory.setLayoutManager(manager);
            parcelHistory.setAdapter(new ParcelHistoryAdapter(setUpParcelList()));

        }else{
            showMessage.setVisibility(View.VISIBLE);
            showMessage.setText("You havent done anything so far to your couriers");
        }
    }

    private void setClickListener(){
       /* fromDateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
    }
    public List<Courier> setUpParcelList() {

        List<Courier> courierList = new ArrayList<>();
        courierList=databse.courierDAO().getCourierListOrderFulfilled();
        return courierList;

    }



}
